﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Sirenix.OdinInspector;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class TilePool : MonoBehaviour {

    public GameObject HandTemplate;
    public Transform HandParentTransform;
    public GameObject BoardTemplate;
    public Transform BoardParentTransform;
    public int NumToGenerateOnAwake;

    private readonly List<HandTileController> handTiles = new List<HandTileController>();
    private readonly List<BoardTileController> boardTiles = new List<BoardTileController>();
    
    private void Awake() {
        GameContext.TilePool = this;
        for (int i = 0; i < NumToGenerateOnAwake; i++) {
            InstantiateHandTileToPool();
        }
        for (int i = 0; i < NumToGenerateOnAwake; i++) {
            InstantiateBoardTileToPool();
        }
    }

    /// <summary>
    /// Returns an INACTIVE tile object.
    /// </summary>
    /// <returns></returns>
    public HandTileController GrabHandTile() {
        if (handTiles.Count < 1) {
            InstantiateHandTileToPool();
        }
        HandTileController returnedHandTile = handTiles[0];
        handTiles.RemoveAt(0);
        return returnedHandTile;
    }

    public void ReturnHandTileToPool(HandTileController returnedHandTile) {
        returnedHandTile.gameObject.name = "Hand Tile - Pooled " + handTiles.Count.ToString("D2");
        handTiles.Add(returnedHandTile);
        returnedHandTile.gameObject.SetActive(false);
    }

    private void InstantiateHandTileToPool () {
        GameObject newTile = Instantiate(HandTemplate, HandParentTransform);
        newTile.name = "Hand Tile - Pooled " + handTiles.Count.ToString("D2");
        HandTileController handTileController = newTile.GetComponent<HandTileController>();
        handTiles.Add(handTileController);
    }
    
    /// <summary>
    /// Returns an INACTIVE tile object.
    /// </summary>
    /// <returns></returns>
    public BoardTileController GrabBoardTile() {
        if (boardTiles.Count < 1) {
            InstantiateBoardTileToPool();
        }
        BoardTileController returnedBoardTile = boardTiles[0];
        boardTiles.RemoveAt(0);
        return returnedBoardTile;
    }

    public void ReturnBoardTileToPool(BoardTileController returnedBoardTile) {
        returnedBoardTile.gameObject.name = "Board Tile - Pooled " + boardTiles.Count.ToString("D3");
        boardTiles.Add(returnedBoardTile);
        returnedBoardTile.gameObject.SetActive(false);
    }

    private void InstantiateBoardTileToPool () {
        GameObject newTile = Instantiate(BoardTemplate, BoardParentTransform);
        newTile.name = "Board Tile - Pooled " + boardTiles.Count.ToString("D3");
        BoardTileController boardTileController = newTile.GetComponent<BoardTileController>();
        boardTiles.Add(boardTileController);
    }
    
}
