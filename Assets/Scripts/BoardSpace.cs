﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public enum TilePlacingStatus {
    empty,          // No tile is being placed here.
    placing,        // A tile is being placed here, but the user can still cancel.
    set             // A tile has been placed here in the past, and is now set.
}

[System.Serializable]
public class BoardSpace  {
    
    public GameObject RootObject;
    public BoardSpaceDisplayer Displayer;
    public Transform DisplayerTransform => Displayer.transform;
    public readonly int Column;
    public readonly int Row;

    public TileController TileController => BoardTileController != null ? (TileController) BoardTileController : tileBeingPlaced;
    public BoardTileController BoardTileController;
    public TilePlacingStatus PlacingStatus = TilePlacingStatus.empty;
    
    private BoardManager board;

    // Neighbors
    public BoardSpace LeftNeighbor => board.SpaceAtCoordinates(Column - 1, Row);
    public BoardSpace RightNeighbor => board.SpaceAtCoordinates(Column + 1, Row);
    public BoardSpace UpNeighbor => board.SpaceAtCoordinates(Column, Row + 1);
    public BoardSpace DownNeighbor => board.SpaceAtCoordinates(Column, Row - 1);
    
    // Tile Being Placed
    private HandTileController tileBeingPlaced;

    public BoardSpace(BoardManager owningBoard, int internalColumn, int internalRow) {
        board = owningBoard;
        Column = internalColumn + board.ExternalColumnOffset;
        Row = internalRow + board.ExternalRowOffset;
    }

    public void StartPlacingTile(HandTileController tile) {
        tileBeingPlaced = tile;
        PlacingStatus = TilePlacingStatus.placing;
    }

    public void UndoPlacingTile () {
        tileBeingPlaced = null;
        PlacingStatus = TilePlacingStatus.empty;
    }

    public void SetTile(BoardTileController boardTileController) {
        BoardTileController = boardTileController;
        Transform tileTransform = BoardTileController.gameObject.transform;
        tileTransform.parent = DisplayerTransform;
        tileTransform.position = new Vector3(DisplayerTransform.position.x, DisplayerTransform.position.y, ObjectZs.BoardTiles);
    }

    public void RegisterClick() {
        
        // Generally, the only meaningful thing that happens when a user clicks a board space is to place a tile there
        // if a tile is being dragged via a click selection. Try placing a tile.
        GameContext.FightManager.PlaceATile(this);
        
    }

    /// <summary>
    /// Get all the spaces connected horizontally with this space that contain a tile.
    /// </summary>
    public List<BoardSpace> GetHorizontallyConnectedSpacesWithTiles (List<SpaceBeingResolved> virtualTiles = null) {
        
        if (virtualTiles == null) {virtualTiles = new List<SpaceBeingResolved>();}
        
        // Go through connected spaces to the left to find the first connected space with a tile.
        BoardSpace furthestLeftSoFar = this;
        while (true) {
            if (furthestLeftSoFar.LeftNeighbor != null && (furthestLeftSoFar.LeftNeighbor.TileController != null || virtualTiles.Any(e => e.BoardSpace == furthestLeftSoFar.LeftNeighbor))) {
                furthestLeftSoFar = furthestLeftSoFar.LeftNeighbor;
            }
            else { break; }
        }
        
        // Starting with that space, proceed rightward until the last connected space with a tile.
        List<BoardSpace> spaces = new List<BoardSpace>();
        BoardSpace furthestRightSoFar = furthestLeftSoFar;
        while (true) {
            spaces.Add(furthestRightSoFar);
            if (furthestRightSoFar.RightNeighbor != null && (furthestRightSoFar.RightNeighbor.TileController != null || virtualTiles.Any(e => e.BoardSpace == furthestRightSoFar.RightNeighbor))) {
                furthestRightSoFar = furthestRightSoFar.RightNeighbor;
            }
            else { break; }
        }

        return spaces;

    }
    
    /// <summary>
    /// Get all the spaces connected vertically with this space that contain a tile.
    /// </summary>
    public List<BoardSpace> GetVerticallyConnectedSpacesWithTiles(List<SpaceBeingResolved> virtualTiles = null) {
        
        if (virtualTiles == null) {virtualTiles = new List<SpaceBeingResolved>();}
        
        // Go through connected spaces upward to find the first connected space with a tile.
        BoardSpace furthestUpSoFar = this;
        while (true) {
            if (furthestUpSoFar.UpNeighbor != null && (furthestUpSoFar.UpNeighbor.TileController != null || virtualTiles.Any(e => e.BoardSpace == furthestUpSoFar.UpNeighbor))) {
                furthestUpSoFar = furthestUpSoFar.UpNeighbor;
            }
            else { break; }
        }
        
        // Starting with that space, proceed downward until the last connected space with a tile.
        List<BoardSpace> spaces = new List<BoardSpace>();
        BoardSpace furthestDownSoFar = furthestUpSoFar;
        while (true) {
            spaces.Add(furthestDownSoFar);
            if (furthestDownSoFar.DownNeighbor != null && (furthestDownSoFar.DownNeighbor.TileController != null || virtualTiles.Any(e => e.BoardSpace == furthestDownSoFar.DownNeighbor))) {
                furthestDownSoFar = furthestDownSoFar.DownNeighbor;
            }
            else { break; }
        }

        return spaces;

    }

    public bool HasAdjacentTile() {
        return (LeftNeighbor != null && LeftNeighbor.TileController != null) ||
               (RightNeighbor != null && RightNeighbor.TileController != null) ||
               (UpNeighbor != null && UpNeighbor.TileController != null) ||
               (DownNeighbor != null && DownNeighbor.TileController != null);
    }

    /// <summary>
    /// Attempts to resolve a word starting on this space.
    /// </summary>
    public BoardResolution TryFitWord (string word, bool isVert) {
        
        // WORD FIT. Check if the word actually fits.
        bool adjacentTileFound = false;
        bool emptyTileFound = false;
        for (int i = 0; i < word.Length; i++) {
            BoardSpace space;
            space = isVert ? board.SpaceAtCoordinates(Column, Row - i) : board.SpaceAtCoordinates(Column + i, Row);
            if (space == null) return null;    // Board must physically fit the word.
            if (space.PlacingStatus != TilePlacingStatus.empty && space.TileController.SelectedLetter() != word[i].ToString()) return null;    // Must not contain any letter clashes.
            if (!adjacentTileFound && space.HasAdjacentTile()) adjacentTileFound = true;    // Must connect to other tiles.
            if (!emptyTileFound && space.TileController == null) emptyTileFound = true;    // Must contain at least one empty space.
        }
        if (!adjacentTileFound || !emptyTileFound) return null;
        
        // RESOLVE. Resolve the board with these letters and return the BoardResolution if it resolves legally.
        List<SpaceBeingResolved> spacesBeingResolved = new List<SpaceBeingResolved>();
        for (int i = 0; i < word.Length; i++) {
            SpaceBeingResolved newSpaceBeingResolved = new SpaceBeingResolved {
                BoardSpace = isVert ? board.SpaceAtCoordinates(Column, Row - i) : board.SpaceAtCoordinates(Column + i, Row),
                LettersBeingTried = word[i].ToString()
            };
            spacesBeingResolved.Add(newSpaceBeingResolved);
        }
        BoardResolution boardResolution = new BoardResolution(spacesBeingResolved, isVert);
        return boardResolution.ResolvesLegally ? boardResolution : null;
        
    }
    
}
