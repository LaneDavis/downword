﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardTileController : TileController {

    public BoardSpace Space { get; set; }
    public string Letter { get; set; }

    // Initialize display from a Fight Tile.
    public void InitializeFromFightTile (FightTile newFightTile, string selectedLetter, BoardSpace setSpace) {
        FightTile = newFightTile;
        Space = setSpace;
        Letter = selectedLetter;
        SetLetters();
    }
    
    // Initialize display from a letter with no associated Fight Tile. This happens when enemies play words and when
    // a multi-letter tile generates its additional board tiles.
    public void InitializeFromLetter(string selectedLetter, BoardSpace setSpace) {
        Space = setSpace;
        Letter = selectedLetter;
        SetLetters();
    }
    
    protected override void SetLetters() {
        LetterText.text = Letter;
    }
    
    public override string SelectedLetter() {
        return Letter;
    }
    
}
