﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(menuName = "Data Object/Enemy Dictionary")]
public class EnemyDictionary : ScriptableObject {

    public List<string> Words;

    [Button]
    public void SortAlphabetically() {
        Words = Words.OrderBy(e => e).ToList();
    }
    
    [Button]
    public void SortByLength() {
        Words = Words.OrderBy(e => e.Length).ToList();
    }
    
}
