﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightButtonActions : MonoBehaviour {

    private void Awake() {
        GameContext.FightButtonActions = this;
    }
    
    public void UndoPlacement () {
        GameContext.FightManager.UndoPlacement();
    }

    public void SetTiles() {
        GameContext.FightManager.SetTiles();
    }
    
}
