﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

// Controls the visuals of a Health Bar.
public class HealthBar : MonoBehaviour {

    // Canvas Anatomy
    public TextMeshProUGUI Text;
    public RectTransform HealthFill;
    public RectTransform DamageFill;

    // Health Calculation
    private int maxHP;
    private int curHP;
    private float PctHP => (float) curHP / maxHP;
    private float curHealthFillPct;
    private float curDamageFillPct;
    
    // Size Calculations
    private Vector2 baseFillSizeDelta;
    private float fillWidthMax;
    
    // Timing
    public float DelayAfterDamageTaken;
    public float DamageFillDeclineRate;
    private float timeOfLastDamage;
    private bool CanDecline => Time.time - DelayAfterDamageTaken > timeOfLastDamage;

    [Header("Editor Testing")]
    public int TestMaxHealthVal;
    public bool DoTestMaxHealth;
    public int TestCurHealthVal;
    public bool DoTestCurHealth;

    private void Start() {
        fillWidthMax = HealthFill.rect.width;
        baseFillSizeDelta = HealthFill.sizeDelta;
    }
    
    public void SetMaxHP(int newMaxHP) {
        maxHP = newMaxHP;
        curHealthFillPct = PctHP;
        Text.text = $"{curHP}/{maxHP}";
    }

    public void SetCurHP(int newCurHP) {
        if (newCurHP < curHP) {
            timeOfLastDamage = Time.time;
        }
        curHP = newCurHP;
        curHealthFillPct = PctHP;
        Text.text = $"{curHP}/{maxHP}";
    }

    private void Update() {

        if (DoTestMaxHealth) {
            DoTestMaxHealth = false;
            SetMaxHP(TestMaxHealthVal);
        }

        if (DoTestCurHealth) {
            DoTestCurHealth = false;
            SetCurHP(TestCurHealthVal);
        }
        
        HealthFill.sizeDelta = SizeDeltaFromTargetPercentage(curHealthFillPct);
        if (curHealthFillPct > curDamageFillPct) {
            curDamageFillPct = curHealthFillPct;
        } else if (CanDecline && curHealthFillPct < curDamageFillPct) {
            curDamageFillPct = Mathf.Max( curDamageFillPct - Time.deltaTime * DamageFillDeclineRate, curHealthFillPct);
        }
        DamageFill.sizeDelta = SizeDeltaFromTargetPercentage(curDamageFillPct);
    }

    private Vector2 SizeDeltaFromTargetPercentage(float targetPercentage) {
        return new Vector2(fillWidthMax * targetPercentage - fillWidthMax, baseFillSizeDelta.y);
    }

}
