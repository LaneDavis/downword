﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using Debug = UnityEngine.Debug;

[ShowOdinSerializedPropertiesInInspector]
public class BoardManager : MonoBehaviour {

    public GameObject SpaceTemplate;
    public Vector2 AreaPerSpace;

    [OdinSerialize]
    public BoardSpace[][] Spaces;
    public int Columns => Spaces?.Length ?? 0;
    public int Rows => Spaces == null || Spaces.Length < 1 ? 0 : Spaces[0]?.Length ?? 0;
    public int ExternalColumnOffset => -Columns / 2;
    public int ExternalRowOffset => -Rows / 2;
    public float Width => Mathf.Max(Columns, 0) * AreaPerSpace.x;
    public float Height => Mathf.Max(Rows, 0) * AreaPerSpace.y;
    public float DiagonalSize => new Vector2(Width, Height).magnitude;

    public BoardSpace CurrentHoveredSpace { get; set; }

    [Header("Test Parameters")]
    public int TestBoardColumns;
    public int TestBoardRows;
    
    [Button("Generate Board from Test Parameters")]
    public void GenerateBoardFromTestParameters() {
        GenerateBoard(TestBoardColumns, TestBoardRows);
    }

    private void Awake() {
        GameContext.BoardManager = this;
        GenerateBoard(TestBoardColumns, TestBoardRows);
    }
    
    private void GenerateBoard(int numColumns, int numRows) {

        // No board generation with illegal column or row count.
        if (numColumns < 1 || numRows < 1) {
            Debug.LogWarning("BoardManager.GenerateBoard called with non positive row or column count");
            return;
        }
        
        // Clear any existing board.
        ClearBoard();
        
        // Activate SpaceTemplate to save calls.
        SpaceTemplate.SetActive(true);
        
        // Populate a new board.
        Spaces = new BoardSpace[numColumns][];
        for (int i = 0; i < Spaces.Length; i++) {
            Spaces[i] = new BoardSpace[numRows];
            for (int j = 0; j < Spaces[i].Length; j++) {
                Spaces[i][j] = new BoardSpace(this, i, j);
            }
        }
        
        // Create Space objects.
        ForEachSpace(space => {
            space.RootObject = Instantiate(SpaceTemplate, transform);
            Vector2 offset = new Vector2(Columns % 2 == 0 ? 0.5f : 0f, Rows % 2 == 0 ? 0.5f : 0f);
            space.RootObject.transform.localPosition = new Vector2(AreaPerSpace.x * (space.Column + offset.x), AreaPerSpace.y * (space.Row + offset.y));
            space.Displayer = space.RootObject.GetComponent<BoardSpaceDisplayer>();
            space.Displayer.Initialize(this, space);
        });
        
        // Deactivate SpaceTemplate.
        SpaceTemplate.SetActive(false);
        
    }

    private void ClearBoard() {
        if (Spaces != null) {
            ForEachSpace(space => DestroyImmediate(space.RootObject));
        }
        Spaces = null;
    }

    private void ForEachSpace(Action<BoardSpace> action) {
        foreach (var t in Spaces) {
            foreach (var t1 in t) {
                action(t1);
            }
        }
    }

    public BoardSpace SpaceAtCoordinates(int column, int row) {
        int columnWithOffset = column - ExternalColumnOffset;
        int rowWithOffset = row - ExternalColumnOffset;
        if (columnWithOffset < 0 || columnWithOffset >= Spaces.Length || rowWithOffset < 0 || rowWithOffset >= Spaces[columnWithOffset].Length) {
            return null;
        }
        return Spaces[columnWithOffset][rowWithOffset];
    }
    
    #region Tile Setting

    /// <summary>
    /// Determine whether a space is legal for tile-placing.
    /// </summary>
    public bool CanPlaceTileOnSpace(BoardSpace space, List<TileReadyToSet> otherTilesBeingSet, out string failureReason) {
        
        // EMPTY — Confirm that the space is indeed available for a tile.
        if (space.PlacingStatus != TilePlacingStatus.empty) {
            failureReason = "That board space is full!";
            return false;
        }
        
        // SINGLE LINE RULE — Confirm that all the tiles being placed are in a straight line.
        List<int> columns = new List<int>();
        List<int> rows = new List<int>();
        foreach (TileReadyToSet tileSpace in otherTilesBeingSet) {
            columns.AddIfUnique(tileSpace.BoardSpace.Column);
            rows.AddIfUnique(tileSpace.BoardSpace.Row);
        }
        columns.AddIfUnique(space.Column);
        rows.AddIfUnique(space.Row);
        if (columns.Count > 1 && rows.Count > 1) {
            failureReason = "All letters placed must be on the same row or column!";
            return false;
        }

        failureReason = "";
        return true;

    }
    
    /// <summary>
    /// Determine whether a collection of tiles are all parts of legal words, and hence legal for setting.
    /// </summary>
    public BoardResolution GetCurrentSuccessfulBoardResolution(List<TileReadyToSet> tiles) {
        
        // DIRECTIONALITY — Figure out whether this is a Horiz or Vert tile collection.
        bool isVert = tiles.Count > 1 && tiles[0].BoardSpace.Row != tiles[1].BoardSpace.Row;

        // ORDER TILES — If Horiz, sort by Column ascending. If Vert, sort by Row descending.
        tiles = !isVert ? tiles.OrderBy(tile => tile.BoardSpace.Column).ToList() :    // Horiz 
            tiles.OrderByDescending(tile => tile.BoardSpace.Row).ToList();            // Vert

        // LETTER COLLECTIONS — Flex tiles can lead to multiple possible resolutions. Duplicate the list to create a
        // separate "universe" for each combination of letters used among flex tiles.
        List<List<SpaceBeingResolved>> tileUniverses = new List<List<SpaceBeingResolved>> {new List<SpaceBeingResolved>()};
        foreach (TileReadyToSet tile in tiles) {
            int universeCountBefore = tileUniverses.Count;
            tileUniverses.Fork(tile.TileController.PossibleLetters().Count);
            for (int i = 0; i < tileUniverses.Count; i++) {
                tileUniverses[i].Add(new SpaceBeingResolved(tile, tile.TileController.PossibleLetters()[i / universeCountBefore]));
            }
        }
        
        // POSSIBLE RESOLUTIONS — Attempt resolving each collection of letters.
        List<BoardResolution> boardResolutions = new List<BoardResolution>();
        foreach (List<SpaceBeingResolved> tileUniverse in tileUniverses) {
            boardResolutions.Add(new BoardResolution(tileUniverse, isVert));
        }
        
        // FIRST ALPHABETICAL — The board resolutions will appear in alphabetical order. Pick the first.
        return boardResolutions.FirstOrDefault(e => e.ResolvesLegally);
        
    }

    /// <summary>
    /// When a tile's "Executing" animation is complete, this is called to instantiate a "board tile" that handles
    /// a tile's appearance on the board.
    /// </summary>
    public void CreateBoardTile (BoardSpace boardSpace, BoardResolution boardResolution) {

        // Pull a tile from the tile pool.
        BoardTileController newBoardTile = GameContext.TilePool.GrabBoardTile();
        
        // Position it at the bag displayer and activate it.
        newBoardTile.transform.position = boardSpace.Displayer.transform.position;
        newBoardTile.gameObject.SetActive(true);
        
        // Tell it what to look like.
        FightTile fightTile = boardSpace.TileController.FightTile;
        string letterToDisplay = boardResolution.StringUsedAtBoardSpace(boardSpace);
        newBoardTile.InitializeFromFightTile(fightTile, letterToDisplay, boardSpace);
        
        // Connect it to the board.
        boardSpace.SetTile(newBoardTile);

    }
        
    #endregion
    #region Enemy Word Fitting

    public string BruteFitWordForTest;
    public List<BoardResolution> FitsFound;
    
    [Button("Test Brute Fit Word")]
    public void TestBruteFitWord() {
        Stopwatch sw = new Stopwatch();
        sw.Start();
        FitsFound = BruteFitWord(BruteFitWordForTest);
        sw.Stop();;
        Debug.Log(sw.ElapsedMilliseconds);
    }
    
    public List<BoardResolution> BruteFitWord(string word) {

        List<BoardResolution> legalResolutions = new List<BoardResolution>();
        
        foreach (BoardSpace[] column in Spaces) {
            foreach (BoardSpace space in column) {
                BoardResolution horizResolution = space.TryFitWord(word, false);
                if (horizResolution != null) legalResolutions.Add(horizResolution);
                BoardResolution vertResolution = space.TryFitWord(word, true);
                if (vertResolution != null) legalResolutions.Add(vertResolution);
            }
        }

        return legalResolutions;

    }
    
    #endregion
    
}
