﻿using UnityEngine;

/// <summary>
/// Controls the animation state of a small, dot-shaped piece of UI.
/// </summary>
public class DotPopController : MonoBehaviour {

	private const string PopInString = "Dot Pop In";
	private const string PopOutString = "Dot Pop Out";
	private const string IdleString = "Dot Idle";
	private const string HiddenString = "Dot Hidden";
	
	private Animator animator;
	private bool visible;

	public bool StartVisible;
	public float AnimationSpeed = -1f;

	private void Awake() {
		animator = GetComponent<Animator>();
	}

	private void Start() {
		if (StartVisible) {
			PopIn();
		}

		if (AnimationSpeed >= 0f) {
			animator.speed = AnimationSpeed;
		} 
	}

	public void PopIn(bool instant = false) {
		if (!visible) {
			visible = true;
			if (animator != null) {
				animator.enabled = true;
				animator.Play(instant ? IdleString : PopInString);
			}
			else {
				Debug.LogWarning("Animator is null but still trying to access it");
			}
		}
	}
	
	public void PopOut(bool instant = false) {
		if (visible) {
			visible = false;
			if (animator != null) {
				animator.enabled = true;
				animator.Play(instant ? HiddenString : PopOutString);
			}
			else {
				Debug.LogWarning("Animator is null but still trying to access it");
			}
		}
	}

	public void SetAnimationSpeed(float speed) {
		animator.speed = speed;
	}
	
}
