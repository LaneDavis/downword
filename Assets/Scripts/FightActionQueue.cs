﻿using System;
using System.Collections.Generic;

public class FightActionQueue {

    private List<FightAction> queue = new List<FightAction>();
    private FightAction currentFightAction = null;
    
    public void AddAction(Action action, float duration) {
        queue.Add(new FightAction(action, duration));
        TryNextActionInQueue();
    }

    // TODO: If the player or last enemy dies, stop executing.
    private void TryNextActionInQueue() {
        if (currentFightAction != null) return;
        if (queue.Count < 1) return;
        currentFightAction = queue[0];
        queue.RemoveAt(0);
        currentFightAction.Action.Invoke();
        ThreadingUtil.Instance.RunLater(() => {
            currentFightAction = null;
            TryNextActionInQueue();
        }, currentFightAction.Duration);
    }
    
}

public class FightAction {

    public Action Action;
    public float Duration;

    public FightAction(Action action, float duration) {
        Action = action;
        Duration = duration;
    }
}