﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Holds references to everything that is essentially a singleton. This is a convenience measure that gives me all the
/// freedom to rapid-prototype in a world of singletons, but then more easily transition to dependency-injection if
/// that ever seems like a good idea.
/// </summary>
public static class GameContext {

    // Permanent Controllers
    public static PlayerStatus PlayerStatus;
    public static CommandManager CommandManager;
    public static WordManager WordManager;
    public static TilePool TilePool;
    public static PlayerInputManager PlayerInputManager;
    public static EnemyWordManager EnemyWordManager;
    
    // Permanent Display Elements
    public static Camera MainCamera;
    public static MainCameraController MainCameraController;
    public static BagDisplayer BagDisplayer;
    public static DiscardPileDisplayer DiscardPileDisplayer;
    public static HandController HandController;
    public static CanvasAtlas CanvasAtlas;
    public static FightButtonActions FightButtonActions;
    
    // Initialized Per Fight
    public static FightManager FightManager;
    public static FightDisplayManager FightDisplayManager;
    public static TileBag TileBag;
    public static DiscardPile DiscardPile;
    public static BoardManager BoardManager;
    
    // Test Only
    public static TestRunManager TestRunManager;

}
