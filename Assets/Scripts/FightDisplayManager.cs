﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FightDisplayManager : MonoBehaviour {

    private EntityDisplayer activePlayerDisplayer;
    private List<EntityDisplayer> activeEnemyDisplayers = new List<EntityDisplayer>();
    
    private void Awake() {
        GameContext.FightDisplayManager = this;
    }

    public EntityDisplayer SpawnPlayer(PlayerStatus playerStatus) {
        GameObject newPlayer = Instantiate(playerStatus.Character.DisplayPrefab);
        activePlayerDisplayer = newPlayer.GetComponent<EntityDisplayer>();
        return activePlayerDisplayer;
    }

    public EntityDisplayer SpawnEnemy(EnemyData enemyData) {
        GameObject newEnemy = Instantiate(enemyData.DisplayPrefab);
        EntityDisplayer newEnemyDisplayer = newEnemy.GetComponent<EntityDisplayer>();
        activeEnemyDisplayers.Add(newEnemyDisplayer);
        return newEnemyDisplayer;
    }
    
}
