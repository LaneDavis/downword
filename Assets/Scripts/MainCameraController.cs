﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class MainCameraController : MonoBehaviour {

    private Camera camera;
    
    public void Awake() {
        GameContext.MainCamera = camera = GetComponent<Camera>();
        GameContext.MainCameraController = this;
    }

    public float OrthographicHeight () {
        return camera.orthographicSize * 2f;
    }

    public float OrthographicWidth () {
        return camera.orthographicSize * 2f * Screen.width / Screen.height;
    }
    
}
