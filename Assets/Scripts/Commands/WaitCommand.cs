﻿using UnityEngine;

/// <summary>
/// A Command that simply imposes a delay before doing anything else.
/// </summary>
public class WaitCommand : Command {

    public float Duration;
    private float timer;
    
    public override void StartExecution() {
        // Do Nothing.
    }

    public override void Update() {
        timer += Time.deltaTime;
    }

    public override bool IsFinished => timer > Duration;

}
