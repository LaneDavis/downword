﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityEffectCommand : Command {

    public EntityController Owner;
    public List<EntityController> Targets;
    public EntityEffect EntityEffect;

    public float Duration;
    private float timer;
    
    // Update is called once per frame
    public override void StartExecution() {
        
        // DEAD OWNER? If the owner has died, stop execution and finish this command immediately.
        if (Owner == null || Owner.IsDying) {
            timer = Duration;
            return;
        }
        
        // PROCESSING. Process it with any buffs on the owner.
        EntityEffect processedEffect = Owner.ProcessOutgoingEntityEffect(EntityEffect);
        
        // DELIVERY. Send the effect to the target.
        foreach (EntityController target in Targets) {
            target.ReceiveIncomingEntityEffect(processedEffect);
        }
        
    }

    public override void Update() {
        timer += Time.deltaTime;
    }

    public override bool IsFinished => timer > Duration;
}
