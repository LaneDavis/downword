﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CommandManager : MonoBehaviour {
    
    private List<Command> commands = new List<Command>();
    private Command currentCommand;

    private void Awake() {
        GameContext.CommandManager = this;
    }

    public void EnqueueCommand(Command newCommand) {
        commands.Add(newCommand);
    }

    public void EnqueueAfterCurrent(Command newCommand) {
        commands.Insert(0, newCommand);
    }

    public void EnqueueAfterCurrent (List<Command> newCommands) {
        for (int i = newCommands.Count - 1; i >= 0; i--) {    // Enqueue commands in reverse order so they end up in the correct order.
            EnqueueAfterCurrent(newCommands[i]);
        }
    }

    private void Update() {

        // ONGOING COMMAND. If the current command isn't finished, keep executing it.
        if (currentCommand != null && !currentCommand.IsFinished) {
            currentCommand.Update();
            return;
        }
        
        // COMPLETING COMMAND. If the current command is finished, don't keep it around.
        if (currentCommand != null && currentCommand.IsFinished) {
            currentCommand = null;
        }

        // NO COMMAND. No current command, and no other commands to tee up. Do absolutely nothing.
        if (currentCommand == null && commands.Count < 1) {
            return;
        }

        // NEW COMMAND. Either the previous command has finished or a new command has been enqueued onto an empty
        // queue. Begin execution of the new command.
        currentCommand = commands[0];
        commands.RemoveAt(0);
        currentCommand.StartExecution();

    }
    
}
