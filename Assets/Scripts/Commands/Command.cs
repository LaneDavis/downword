﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Command {

    public abstract void StartExecution();
    public abstract void Update();
    public abstract bool IsFinished { get; }

}
