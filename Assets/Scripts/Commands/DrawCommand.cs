﻿using UnityEngine;

public class DrawCommand : Command {

    public float Duration = ActionDurations.TileDraw;
    private float timer;

    public override void StartExecution() {
        GameContext.FightManager.DrawATile();
    }

    public override void Update() {
        timer += Time.deltaTime;
    }

    public override bool IsFinished => timer >= Duration;
}
