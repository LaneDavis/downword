﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionCommand : Command {

    public Action Action;
    public float Duration;
    private float timer;

    public ActionCommand(Action action, float duration) {
        Action = action;
        Duration = duration;
    }
    
    public override void StartExecution() {
        Action?.Invoke();
    }

    public override void Update() {
        timer += Time.deltaTime;
    }

    public override bool IsFinished => timer >= Duration;
}
