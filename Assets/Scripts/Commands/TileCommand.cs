﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles a single tile setting, then queues commands that actually execute the tile effects.
/// </summary>
public class TileCommand : Command {

    private FightManager FightManager => GameContext.FightManager;
    
    public BoardResolution BoardResolution;
    public BoardSpace BoardSpace;
    public HandTileController TileController;

    private TileEffectData.EffectTarget target;
    // TODO: Graphic.
    // TODO: SFX.

    private float Duration;
    private float timer;

    public override void StartExecution() {
        
        // SET DURATION.
        Duration = ActionDurations.TileCommandDuration;
        
        // ENTITY EFFECTS.
        // List setup.
        List<EntityEffectCommand> entityEffectCommands = new List<EntityEffectCommand>();
        
        // For each tile effect...
        foreach (TileEffectData effectData in TileController.TileData.Effects) {
            EntityEffectCommand newCommand = new EntityEffectCommand();    // Create a command.
            newCommand.Duration = effectData.Duration;                     // Set its duration.
            newCommand.Owner = GameContext.FightManager.PlayerEntity;      // Set its owner.
            
            // Set Targeting
            switch (effectData.Target) {
                case TileEffectData.EffectTarget.Self: newCommand.Targets = new List<EntityController> { FightManager.PlayerEntity }; break;
                case TileEffectData.EffectTarget.AllEnemies: newCommand.Targets = FightManager.GetEnemyEntityControllers(); break;
                case TileEffectData.EffectTarget.SingleEnemy: newCommand.Targets = new List<EntityController> { FightManager.TargetEnemyFromBoardSpace(BoardSpace) }; break;
                case TileEffectData.EffectTarget.RandomEnemy: newCommand.Targets = new List<EntityController> { FightManager.GetRandomEnemy() }; break;
            }
            
            // Set Entity Effect
            newCommand.EntityEffect = effectData.ExecutionEffect();
            
            // Add to entityEffectCommands list.
            entityEffectCommands.Add(newCommand);

        }
        
        // MULTI-EXECUTIONS. If the tile is part of two words, it gets executed twice.
        entityEffectCommands.ForkInPlace(BoardResolution.ExecutionsForTile(TileController));
        
        // Send Commands to the Command Manager.
        for (int i = entityEffectCommands.Count - 1; i >= 0; i--) {    // Enqueue commands in reverse order so they end up in the correct order.
            GameContext.CommandManager.EnqueueAfterCurrent(entityEffectCommands[i]);
        }
        
        // Start Tile Animation.
        TileController.StartSettingExecuting();
        
    }

    public override void Update() {
        timer += Time.deltaTime;
    }

    public override bool IsFinished => timer > Duration;

}
