﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestRunManager : MonoBehaviour {

    public FightData TestFightData;

    private void Awake() {
        GameContext.TestRunManager = this;
    }

    public void Start() {
        FightManager newFightManager = new FightManager();
        StartCoroutine( newFightManager.StartFight(TestFightData));
    }

}
