﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class TileTextGenerator {

    private FightTile tile;
    private EntityController playerEntityController;
    
    public TileTextGenerator(FightTile newTile) {
        tile = newTile;
        playerEntityController = GameContext.FightManager.PlayerEntity;
    }

    public string GetText() {
        
        // Set String Variables
        string damageString, blockString, instantString, ongoingString;
        damageString = blockString = instantString = ongoingString = "";
        
        // Set Effect Variables
        List<int> damageInstances = new List<int>();
        List<int> blockInstances = new List<int>();
        bool hasInstantEffect = false;
        bool hasOngoingEffect = false;
        
        // Gather Effects
        foreach (TileEffectData effectData in tile.TileData.Effects) {
            if (effectData is TileEffectDamage damage) {
                damageInstances.Add(damage.Amount);
            }
            if (effectData is TileEffectBlock block) {
                blockInstances.Add(block.Amount);
            }
            hasInstantEffect = hasInstantEffect || effectData.HasInstantEffect;
            hasOngoingEffect = hasOngoingEffect || effectData.HasOngoingEffect;
        }
        
        // Boost Damage Effects by Strength
        damageInstances.ForEach(e => e += playerEntityController.Strength);
        
        // Boost Block Effects by Dexterity
        blockInstances.ForEach(e => e += playerEntityController.Dexterity);
        
        // Parse Damage to String
        if (damageInstances.Count == 1) {
            damageString = $"{damageInstances[0]}{TextSprites.Damage}";
        }
        else if (damageInstances.Count > 1) {
            // Create dictionary to group damage instances by damage amount.
            Dictionary<int, int> damageGroups = new Dictionary<int, int>();
            foreach (int instance in damageInstances) {
                if (damageGroups.ContainsKey(instance)) {
                    damageGroups[instance]++;
                }
                else {
                    damageGroups.Add(instance, 1);
                }
            }

            int idx = 0;
            foreach (KeyValuePair<int, int> group in damageGroups) {
                switch (group.Value) {
                    case 0: continue;
                    case 1:
                        if (idx > 0) damageString += damageString + ", ";
                        damageString += $"{group.Key}{TextSprites.Damage}";
                        idx++;
                        break;
                    default:
                        if (idx > 0) damageString += ", ";
                        damageString += $"{group.Key}{TextSprites.Damage}x{group.Value}";
                        break;
                }
            }
        }
        
        // Parse Block to String
        if (blockInstances.Count == 1) {
            blockString = $"{blockInstances[0]}{TextSprites.Block}";
        }
        else if (blockInstances.Count > 1) {
            // Create dictionary to group damage instances by damage amount.
            Dictionary<int, int> blockGroups = new Dictionary<int, int>();
            foreach (int instance in blockInstances) {
                if (blockGroups.ContainsKey(instance)) {
                    blockGroups[instance]++;
                }
                else {
                    blockGroups.Add(instance, 1);
                }
            }

            int idx = 0;
            foreach (KeyValuePair<int, int> group in blockGroups) {
                switch (group.Value) {
                    case 0: continue;
                    case 1:
                        if (idx > 0) blockString += blockString + ", ";
                        blockString += $"{group.Key}{TextSprites.Block}";
                        idx++;
                        break;
                    default:
                        if (idx > 0) blockString += ", ";
                        blockString += $"{group.Key}{TextSprites.Block}x{group.Value}";
                        break;
                }
            }
        }
        
        // Instant Effect String
        if (hasInstantEffect) instantString = TextSprites.InstantEffect;
        
        // Ongoing Effect String
        if (hasOngoingEffect) ongoingString = TextSprites.OngoingEffect;
        
        // Build Final String
        StringBuilder sb = new StringBuilder();
        sb.Append(damageString);
        if (!string.IsNullOrWhiteSpace(blockString)) {
            if (sb.Length > 0) sb.Append(", ");
            sb.Append(blockString);
        }
        if (!string.IsNullOrWhiteSpace(instantString)) {
            if (sb.Length > 0) sb.Append(", ");
            sb.Append(instantString);
        }

        if (!string.IsNullOrWhiteSpace(ongoingString)) {
            if (sb.Length > 0) sb.Append(", ");
            sb.Append(ongoingString);
        }

        // Return Final String
        return sb.ToString();

    }
    
}
