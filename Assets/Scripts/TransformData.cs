﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformData {

    public Vector3 Position;
    public Vector3 LocalScale;
    public Quaternion Rotation;

    public TransformData() { }

    public TransformData(Transform t) {
        Position = t.position;
        LocalScale = t.localScale;
        Rotation = t.rotation;
    }
    
}
