﻿using System.Collections.Generic;
using UnityEngine;

public static class ListExtensions {
    
    public static void Shuffle<T> (this List<T> list) {
        int n = list.Count;
        for (int i = 0; i < n; i++) {
            int r = i + Random.Range(0, (n - i));
            T t = list[r];
            list[r] = list[i];
            list[i] = t;
        }
    }

    public static T RandomElement<T> (this List<T> list) {
        return list[Random.Range(0, list.Count)];
    }

    public static void RemoveLast<T>(this List<T> list) {
        if (list.Count > 0) {
            list.RemoveAt(list.Count - 1);
        }
    }

    public static void AddIfUnique<T>(this List<T> list, T newElement) {
        if (list.Contains(newElement)) return;
        list.Add(newElement);
    }

    public static void Fork<T>(this List<T> list, int copies) {
        List<T> originalList = new List<T>();
        list.ForEach(e => originalList.Add(e));
        for (int i = 0; i < copies - 1; i++) {
            foreach (T element in originalList) {
                list.Add(element);
            }
        }
    }
    
    public static void ForkInPlace<T>(this List<T> list, int copies) {
        List<T> originalList = new List<T>();
        list.ForEach(e => originalList.Add(e));
        for (int i = 0; i < copies - 1; i++) {
            for (int j = 0; j < originalList.Count; j++) {
                list.Insert(j * (i + 2), originalList[j]);
            }
        }
    }
    
}