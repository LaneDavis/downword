﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ActionDurations {

    public const float TileDraw = 0.125f;

    public const float TileSettingWait = .75f;

    public const float TileCommandDuration = .5f;

    public const float PlayerEndOfTurn = 0.33f;
    public const float PlayerEndOfTurnDiscard = 0.33f;

}
