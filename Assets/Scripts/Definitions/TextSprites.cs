﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Defines sprites used to express common ideas like Damage, Block, Effects, Currency, etc.
/// </summary>
public static class TextSprites {

    public const string Block = "<sprite=0>";
    public const string Damage = "<sprite=1>";
    public const string InstantEffect = "<sprite=2>";
    public const string OngoingEffect = "<sprite=3>";

}
