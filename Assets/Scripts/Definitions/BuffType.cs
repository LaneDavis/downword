﻿public enum BuffType {
    
    // Each Strength increases attack damage by 1.
    Strength = 1,
    
    // Each Dexterity increases block applied by tiles by 1.
    Dexterity = 2,
    
}
