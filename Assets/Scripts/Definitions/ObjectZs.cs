﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ObjectZs {

    // Board Spaces
    public const float BoardSpaces = 0f;
    
    // Tiles on Board
    public const float ReadyToPlaceTiles = -2.1f;
    public const float BoardTiles = -1f;

    // Tiles in Hand
    public const float HandTiles = -2f;
    public const float HandTilePerIndexOffset = 0.001f;
    public const float HandTileHoveredOffset = -0.01f;
    public const float HandTileDraggedOffset = -0.02f;

}
