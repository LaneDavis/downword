﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInitialization : MonoBehaviour {

    public CharacterData TestCharacter;
    
    private void Awake() {
        GameContext.PlayerStatus = new PlayerStatus();
        GameContext.PlayerStatus.ImportCharacterData(TestCharacter);
    }
    
}
