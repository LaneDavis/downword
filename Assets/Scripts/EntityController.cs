﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityController {

    public int MaxHP;
    public int CurHP;
    public delegate void DamageFinalized(int amount);

    public int Block;
    
    public Dictionary<BuffType, int> Buffs = new Dictionary<BuffType, int>();
    public int Strength => Buffs.ContainsKey(BuffType.Strength) ? Buffs[BuffType.Strength] : 0;
    public int Dexterity => Buffs.ContainsKey(BuffType.Dexterity) ? Buffs[BuffType.Dexterity] : 0;
    
    // DELEGATES
    public delegate void BlockGained(int amount);
    public event BlockGained OnBlockGained;
    public delegate void DamageBlocked(int amount);
    public event DamageBlocked OnDamageBlocked;
    public delegate void HealthLost(int amount);
    public event HealthLost OnHealthLost;
    public delegate void Death();
    public event Death OnDie;
    
    // DISPLAY
    public EntityDisplayer EntityDisplayer;
    
    // ENEMY ONLY. EnemyData.
    public EnemyData EnemyData;
    
    // TODO: Death Animation.
    public bool IsDying { get; private set; }
    

    // PLAYER - Initialize from Player Status
    public EntityController(PlayerStatus playerStatus) {
        MaxHP = playerStatus.MaxHP;
        CurHP = playerStatus.CurHP;
    }

    // ENEMY - Initialize from Enemy Data
    public EntityController(EnemyData enemyData) {
        EnemyData = enemyData;
        MaxHP = CurHP = enemyData.MaxHP;
    }

    public EntityEffect ProcessOutgoingEntityEffect(EntityEffect processedEffect) {

        processedEffect.DamageApplied += Strength;
        processedEffect.BlockApplied += Dexterity;
        return processedEffect;

    }

    public void ReceiveIncomingEntityEffect(EntityEffect receivedEffect) {
        
        // HANDLE DAMAGE.
        if (receivedEffect.DamageApplied > 0) {
            TakeDamage(receivedEffect.DamageApplied);
        }
        
        // APPLY BLOCK.
        if (receivedEffect.BlockApplied > 0) {
            GainBlock(receivedEffect.BlockApplied);
        }
        
    }

    public void TakeDamage(int damageAmount) {

        int blockLost = Math.Min(damageAmount, Block);
        if (blockLost > 0 && damageAmount > 0) {
            OnDamageBlocked?.Invoke(blockLost);
        }
        damageAmount -= blockLost;
        if (damageAmount < 1) {
            // TODO: Illustrate full block.
            return;
        }
        int healthLost = Math.Min(CurHP, damageAmount);
        OnHealthLost?.Invoke(healthLost);
        CurHP -= damageAmount;
        EntityDisplayer.SetHealthBar(CurHP, MaxHP);
        if (CurHP <= 0) {
            Die();
        }
        else {
            EntityDisplayer.DoDamageTaken();
        }

    }

    public void GainBlock(int blockAmount) {

        Block += blockAmount;

    }

    public void Die() {
        
        OnDie?.Invoke();
            
    }
    
}
