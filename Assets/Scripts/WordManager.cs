﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WordManager : MonoBehaviour {

    private string[] words;
    public List<TextAsset> WordHunks;
    public float SecondsPerHunk;

    // Loading
    private enum LoadState { NotLoading, Loading, Finished }
    private LoadState loadState = LoadState.NotLoading;
    private int hunksLoaded;
    private float timeOfNextHunk;

    // Word Searching
    private int recursionSafety;
    public string WordCheck;
    
    // Delegates
    private List<Action> OnLoadCompletes = new List<Action>();

    private void Awake () {
        GameContext.WordManager = this;
    }
    
    private void Start() {
        words = new string[0];
    }

    public bool CheckWord(string word) {
        recursionSafety = 100;
        int result = CheckWord(word, 0, words.Length - 1);
        return result != -1;
    }

    public void RunWhenLoadFinished (Action onComplete) {
        if (loadState == LoadState.Finished) onComplete?.Invoke();
        else OnLoadCompletes.Add(onComplete);
    }
    
    private void Update() {
        if (loadState == LoadState.NotLoading && Time.time > 1f) {
            loadState = LoadState.Loading;
        }

        if (loadState == LoadState.Loading) {
            if (WordHunks.Count > hunksLoaded) {
                if (Time.time > timeOfNextHunk) {
                    LoadNextHunk();
                    timeOfNextHunk = Time.time + SecondsPerHunk;
                }
            }
            else {
                loadState = LoadState.Finished;
                OnLoadCompletes.ForEach(e => e?.Invoke());
                OnLoadCompletes.Clear();
            }
        }
    }

    private void LoadNextHunk() {
        string allWords = WordHunks[hunksLoaded].ToString();
        string[] hunkWords;
        hunkWords = allWords.Split(
            new[] { Environment.NewLine },
            StringSplitOptions.None
        );
        words = words.Concat(hunkWords).ToArray();
        hunksLoaded++;
    }

    private int CheckWord(string word, int lower, int upper) {
        recursionSafety--;
        if (recursionSafety < 1) {
            return -1;
        }
        int mid = (lower + upper) / 2;
        if (mid == lower) {
            if (word.Equals(words[mid])) return mid;
            if (word.Equals(words[upper])) return upper;
            return -1;
        }
        string comparedWord = words[mid];
        int comparison = String.CompareOrdinal(word, comparedWord);
        if (comparison > 0) {
            return CheckWord(word, mid, upper);
        }
        if (comparison < 0) {
            return CheckWord(word, lower, mid);
        }
        return mid;
    }

}
