﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LogUtil {

    /// <summary>
    /// Log a message to the unity console in blue.
    /// </summary>
    /// <param name="message">The text of the message.</param>
    public static void LogGameMessage(string message) {
        Debug.Log($"<b><color=#71aeff>{message}</color></b>");
    }
    
}
