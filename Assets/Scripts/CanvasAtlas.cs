﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasAtlas : MonoBehaviour {

    // Fight Buttons
    public FightButtonController UndoFightButton;
    public FightButtonController SetFightButton;

    private void Awake() {
        GameContext.CanvasAtlas = this;
    }

}
