﻿using UnityEngine;

public static class TransformExtensions {

    public static void SetZPos(this Transform t, float newZ) {
        Vector3 oldPos = t.position;
        t.position = new Vector3(oldPos.x, oldPos.y, newZ);
    }
    
}
