﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DiscardPile {

    // The tiles in the discard pile. The last tile here is the one most recently added to the bag;
    public List<FightTile> Tiles = new List<FightTile>();
    public DiscardPileDisplayer Displayer;

    public DiscardPile() {
        Displayer = GameContext.DiscardPileDisplayer;
    }
    
    /// <summary>
    /// Adds a tile to the top of the discard pile.
    /// </summary>
    /// <param name="newTile">The tile being added.</param>
    public void AddTile (FightTile newTile) {
        Tiles.Add(newTile);
    }

    /// <summary>
    /// Empties the discard pile, returning all tiles as a list. This is done when the player tries to draw a tile
    /// with an empty bag.
    /// </summary>
    /// <returns>All tiles that were in the discard pile.</returns>
    public List<FightTile> PullTilesFromDiscardPile() {
        List<FightTile> returnedTiles = Tiles.ToList();
        Tiles.Clear();
        return returnedTiles;
    }
    
}
