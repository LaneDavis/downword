﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A link between the EntityController and all the display elements of an entity.
/// </summary>
public class EntityDisplayer : MonoBehaviour {

    public List<FxController> DamageTakenFX;
    public HealthBar HealthBar;

    public void DoDamageTaken() {
        DamageTakenFX.ForEach(e => e.TriggerAll());
    }

    public void SetHealthBar(int curHP, int maxHP) {
        HealthBar.SetMaxHP(maxHP);
        HealthBar.SetCurHP(curHP);
    }

}
