﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// The player's bag from which new tiles are drawn in combat. Does not exist outside of combat. At the beginning of
/// each combat, all the player's owned tiles are jumbled into the bag.
/// </summary>
public class TileBag {

    // Tiles currently in the player's bag.
    public List<FightTile> Tiles;
    public BagDisplayer Displayer;

    /// <summary>
    /// Create a new tile bag.
    /// </summary>
    /// <param name="initialTiles">Tiles that get shuffled into the bag.</param>
    public TileBag(List<FightTile> initialTiles) {
        Tiles = initialTiles.ToList();
        Tiles.Shuffle();
        Displayer = GameContext.BagDisplayer;
    }

    /// <summary>
    /// Draws a single tile from the tile bag. If null is returned, the discard pile should be shuffled into the bag.
    /// </summary>
    /// <returns>The drawn tile.</returns>
    public FightTile DrawTile() {
        if (Tiles.Count > 0) {
            FightTile returnedTile = Tiles[0];
            Tiles.RemoveAt(0);
            return returnedTile;
        }
        return null;
    }

    /// <summary>
    /// Shuffles new tiles into the bag.
    /// </summary>
    /// <param name="newTiles">The new tiles being added</param>
    public void ShuffleInTiles(List<FightTile> newTiles) {
        foreach (FightTile tile in newTiles) {
            Tiles.Add(tile);
        }
        Tiles.Shuffle();
    }
    
}
