﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Holds data needed to execute a tile combat effect.
/// </summary>
public class TileEffectArgs {

    public PlayerStatus PlayerStatus;
    public EntityController PlayerEntity;
    public EntityController Target;
    public EntityEffect EffectApplied;

}
