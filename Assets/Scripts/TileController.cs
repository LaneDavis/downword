﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class TileController : MonoBehaviour {
    
    public TextMeshProUGUI LetterText;
    
    public FightTile FightTile { get; protected set; }
    public TileData TileData => FightTile.TileData;

    protected virtual void SetLetters() {
        LetterText.text = TileData.Letters;
    }

    public List<string> PossibleLetters() {
        return FightTile.PossibleLetters();
    }

    public virtual string SelectedLetter() {
        return "";
    }
    
}
