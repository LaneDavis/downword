﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpaceBeingResolved  {
    
    public TileController TileController;
    public BoardSpace BoardSpace;
    public string LettersBeingTried = "";

    public SpaceBeingResolved(TileReadyToSet tileReadyToSet, string lettersBeingTried) {
        TileController = tileReadyToSet.TileController;
        BoardSpace = tileReadyToSet.BoardSpace;
        LettersBeingTried = lettersBeingTried;
    }

    public SpaceBeingResolved() {
        
    }
    
}
