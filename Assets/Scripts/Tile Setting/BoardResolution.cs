﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// A hypothetical state of the board used when checking tile setting legality. 
/// </summary>
[Serializable]
public class BoardResolution {

    public bool ResolvesLegally => LegalWords.Count > 0 && IllegalWords.Count < 1;
    
    public List<BoardWord> LegalWords = new List<BoardWord>();
    public List<BoardWord> IllegalWords = new List<BoardWord>();

    public List<SpaceBeingResolved> SpacesBeingResolved;

    public BoardResolution (List<SpaceBeingResolved> tilesBeingResolved, bool isVert) {
        
        // RECORD TILES 
        SpacesBeingResolved = tilesBeingResolved;
        
        // SPACES — Determine which spaces are being placed upon.
        List<BoardSpace> spacesBeingPlaced = tilesBeingResolved.Select(e => e.BoardSpace).ToList();
        
        // GATHER — Grab all horizontal and vertical collections of connected spaces with tiles.
        LegalWords = new List<BoardWord>();
        
        // Horizontal Core Word
        if (!isVert) {
            
            // Core Word
            List<SpaceBeingResolved> coreWord = new List<SpaceBeingResolved>();
            List<BoardSpace> horizontalSpaces = tilesBeingResolved[0].BoardSpace.GetHorizontallyConnectedSpacesWithTiles(SpacesBeingResolved);
            for (int i = horizontalSpaces.Count - 1; i >= 0; i--) {
                if (spacesBeingPlaced.Contains(horizontalSpaces[i])) horizontalSpaces.RemoveAt(i);
            }
            foreach (BoardSpace space in horizontalSpaces) {
                if (!spacesBeingPlaced.Contains(space)) {
                    coreWord.Add(TileBeingResovedFromBoardSpace(space));
                }
            }
            foreach (SpaceBeingResolved tile in tilesBeingResolved) {
                coreWord.Add(tile);
            }
            coreWord = coreWord.OrderBy(e => e.BoardSpace.Column).ToList();
            LegalWords.Add(new BoardWord(coreWord, false));
            
            // Vertical Words branching off of the Core Word
            foreach (SpaceBeingResolved tile in tilesBeingResolved) {
                List<SpaceBeingResolved> verticalWord = new List<SpaceBeingResolved>();
                List<BoardSpace> verticalSpaces = tile.BoardSpace.GetVerticallyConnectedSpacesWithTiles();
                foreach (BoardSpace space in verticalSpaces) {
                    if (!spacesBeingPlaced.Contains(space)) {
                        verticalWord.Add(TileBeingResovedFromBoardSpace(space));
                    }
                }
                verticalWord.Add(tile);
                verticalWord = verticalWord.OrderByDescending(e => e.BoardSpace.Row).ToList();
                LegalWords.Add(new BoardWord(verticalWord, true));
            }
            
        }

        // Vertical Core Word
        else {
            
            // Core Word
            List<SpaceBeingResolved> coreWord = new List<SpaceBeingResolved>();
            List<BoardSpace> verticalSpaces = tilesBeingResolved[0].BoardSpace.GetVerticallyConnectedSpacesWithTiles(SpacesBeingResolved);
            foreach (BoardSpace space in verticalSpaces) {
                if (!spacesBeingPlaced.Contains(space)) {
                    coreWord.Add(TileBeingResovedFromBoardSpace(space));
                }
            }
            foreach (SpaceBeingResolved tile in tilesBeingResolved) {
                coreWord.Add(tile);
            }
            coreWord = coreWord.OrderByDescending(e => e.BoardSpace.Row).ToList();
            LegalWords.Add(new BoardWord(coreWord, true));
            
            // Vertical Words branching off of the Core Word
            foreach (SpaceBeingResolved tile in tilesBeingResolved) {
                List<SpaceBeingResolved> horizontalWord = new List<SpaceBeingResolved>();
                List<BoardSpace> horizontalSpaces = tile.BoardSpace.GetHorizontallyConnectedSpacesWithTiles();
                foreach (BoardSpace space in horizontalSpaces) {
                    if (!spacesBeingPlaced.Contains(space)) {
                        horizontalWord.Add(TileBeingResovedFromBoardSpace(space));
                    }
                }
                horizontalWord.Add(tile);
                horizontalWord = horizontalWord.OrderBy(e => e.BoardSpace.Column).ToList();
                LegalWords.Add(new BoardWord(horizontalWord, false));
            }
            
        }
        
        // MULTI-LETTER RULE — Only keep words with more than one letter.
        for (int i = LegalWords.Count - 1; i >= 0; i--) {
            if (LegalWords[i].TilesBeingResolved.Count < 2) LegalWords.RemoveAt(i);
        }
        
        // CONSECUTIVE RULE — Strike any words that have unconnected tiles.
        for (int i = LegalWords.Count - 1; i >= 0; i--) {
            if (!LegalWords[i].IsConsecutive()) LegalWords.RemoveAt(i);
        }
        
        // UNIQUENESS RULE — Only keep words that don't cover the exact same spaces.
        for (int i = LegalWords.Count - 1; i >= 0; i--) {
            bool removeAtI = false;
            for (int j = i - 1; j >= 0; j--) {
                if (LegalWords[i].Equals(LegalWords[j])) {
                    removeAtI = true;
                    break;
                }
            }
            if (removeAtI) {
                LegalWords.RemoveAt(i);
            }
        }
        
        // LEGAL WORD CHECK — Check that all words are dictionary legal.
        IllegalWords = new List<BoardWord>();
        for (int i = LegalWords.Count - 1; i >= 0; i--) {
            if (!GameContext.WordManager.CheckWord(LegalWords[i].String)) {
                IllegalWords.Add(LegalWords[i]);
                LegalWords.RemoveAt(i);
            }
        }
        
    }

    public int ExecutionsForTile(HandTileController tileController) {
        int count = 0;
        foreach (BoardWord legalWord in LegalWords) {
            if (legalWord.TilesBeingResolved.Any(e => e.TileController == tileController)) {
                count++;
            }
        }
        return count;
    }
    
    public string StringUsedAtBoardSpace (BoardSpace space) {
        SpaceBeingResolved spaceStateAtBoardSpace = SpacesBeingResolved.FirstOrDefault(e => e.BoardSpace == space);
        return spaceStateAtBoardSpace == null ? "" : spaceStateAtBoardSpace.LettersBeingTried;
    }
    
    private SpaceBeingResolved TileBeingResovedFromBoardSpace(BoardSpace boardSpace) {
        return new SpaceBeingResolved {
            TileController = boardSpace.TileController,
            BoardSpace = boardSpace,
            LettersBeingTried = boardSpace.TileController.SelectedLetter()
        };
    }

}
