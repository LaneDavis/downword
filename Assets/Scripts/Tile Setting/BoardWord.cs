﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// A word composed of tiles on the board.
/// </summary>
[System.Serializable]
public class BoardWord {

    public bool IsVert;
    
    public readonly string String;
    public readonly List<SpaceBeingResolved> TilesBeingResolved;

    public BoardWord(List<SpaceBeingResolved> tilesBeingResolved, bool isVert) {

        IsVert = isVert;
        TilesBeingResolved = tilesBeingResolved;
        StringBuilder sb = new StringBuilder();
        foreach (SpaceBeingResolved tbr in tilesBeingResolved) {
            sb.Append(tbr.LettersBeingTried);
        }
        String = sb.ToString();

    }

    public bool IsConsecutive() {

        // Horizontal
        if (!IsVert) {
            List<SpaceBeingResolved> tbrs = TilesBeingResolved.OrderBy(e => e.BoardSpace.Column).ToList();
            int col = tbrs.Min(e => e.BoardSpace.Column);
            for (int i = 0; i < tbrs.Count; i++) {
                if (tbrs[i].BoardSpace.Column != col) return false;
                col++;
            }
        }

        // Vertical
        else {
            List<SpaceBeingResolved> tbrs = TilesBeingResolved.OrderBy(e => e.BoardSpace.Row).ToList();
            int row = tbrs.Min(e => e.BoardSpace.Row);
            for (int i = 0; i < tbrs.Count; i++) {
                if (tbrs[i].BoardSpace.Row != row) return false;
                row++;
            }
        }

        return true;

    }

    public bool Equals(BoardWord other) {
        if ((TilesBeingResolved == null) != (other.TilesBeingResolved == null)) return false;
        if (TilesBeingResolved == null && other.TilesBeingResolved == null) return true;
        if (String != other.String) return false;
        if (TilesBeingResolved.Count != other.TilesBeingResolved.Count) return false;
        foreach (SpaceBeingResolved tbr in TilesBeingResolved) {
            if (!other.TilesBeingResolved.Contains(tbr)) {
                return false;
            }
        }
        return true;
    }

}
