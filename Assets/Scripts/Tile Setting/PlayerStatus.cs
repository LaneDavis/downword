﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerStatus {

    public int MaxHP;
    public int CurHP;
    public List<OwnedTile> OwnedTiles;
    public List<RelicData> OwnedRelics;
    public CharacterData Character;

    public void ImportCharacterData(CharacterData data) {
        MaxHP = CurHP = data.StartingHP;
        OwnedTiles = new List<OwnedTile>();
        data.StartingTiles.ForEach(e => OwnedTiles.Add(e.ToOwnedTile()));
        OwnedRelics = data.StartingRelics.ToList();
        Character = data;
    }

}
