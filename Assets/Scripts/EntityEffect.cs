﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// An effect that is applied to an entity. Tiles and enemy attacks construct EntityEffects and hand them to
/// EntityControllers for the execution.
/// </summary>
public class EntityEffect {

    // DAMAGE. Removes block, then HP.
    public int DamageApplied;
    
    // BLOCK. Temporary health that prevents damage taken.
    public int BlockApplied;
    
    // HEALING. HP restored by the target.
    public int HealingApplied;
    
    // HP LOSS. Direct HP loss incurred to the target.
    public int HPLoss;
    
    // BUFFS. Buffs gained by the target.
    public Dictionary<BuffType, int> BuffsApplied;

}
