﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FightButtonController : MonoBehaviour {

    public DotPopController DotPopController;

    public void Appear() {
        DotPopController.PopIn();
    }

    public void Disappear() {
        DotPopController.PopOut();
    }

}
