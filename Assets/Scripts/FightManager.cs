﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using UnityEngine;

/// <summary>
/// Handles the phases of a fight, passing control to the player and away from the player.
/// </summary>
public class FightManager {

    private FightActionQueue actionQueue;

    private FightDisplayManager FightDisplayManager => GameContext.FightDisplayManager;
    private BoardManager BoardManager => GameContext.BoardManager;
    private TileBag TileBag => GameContext.TileBag;
    private DiscardPile DiscardPile => GameContext.DiscardPile;
    private HandController HandController => GameContext.HandController;
    private CommandManager CommandManager => GameContext.CommandManager;
    private EnemyWordManager EnemyWordManager => GameContext.EnemyWordManager;
    

    public EntityController PlayerEntity;
    public List<EntityController> EnemyEntities;

    public enum GamePhase {
        PlayerTurn,
        PlayerExecution,
        PlayerEndOfTurn,
        EnemyPlacing,
        EnemyExecution
    }

    public GamePhase CurrentPhase;

    // TODO: Delegates for things that happen at the beginning and end of phases.

    // Called on START in TestRunManager.
    public IEnumerator StartFight(FightData data) {;
        
        // Initialize Player Entity
        PlayerEntity = new EntityController(GameContext.PlayerStatus);
        PlayerEntity.EntityDisplayer = FightDisplayManager.SpawnPlayer(GameContext.PlayerStatus);
        ThreadingUtil.Instance.RunLater(() => {
            PlayerEntity.EntityDisplayer.SetHealthBar(GameContext.PlayerStatus.CurHP, GameContext.PlayerStatus.MaxHP);
        });
        
        // Convert owned tiles to FightTiles, which are used to handle within-fight usage and alteration.
        List<FightTile> fightTiles = new List<FightTile>();
        GameContext.PlayerStatus.OwnedTiles.ForEach(e => fightTiles.Add(new FightTile(e)));
        
        // Initialize Combat-Relevant handlers.
        GameContext.TileBag = new TileBag(fightTiles);
        GameContext.DiscardPile = new DiscardPile();
        GameContext.FightManager = this;
        actionQueue = new FightActionQueue();

        // Initialize Enemy Entities
        EnemyEntities = new List<EntityController>();
        foreach (EnemyData enemy in data.Enemies) {
            EntityController newEntityController = new EntityController(enemy);
            EnemyEntities.Add(newEntityController);
            newEntityController.EntityDisplayer = FightDisplayManager.SpawnEnemy(enemy);
        }
        
        // Initialize Enemy Word Found Delegates
        EnemyWordManager.OnWordFound -= OnEnemyWordFound; EnemyWordManager.OnWordFound += OnEnemyWordFound;
        EnemyWordManager.OnNoWordFound -= OnEnemyNoWordFound; EnemyWordManager.OnNoWordFound += OnEnemyNoWordFound;
        
        bool waiting = true;
        GameContext.WordManager.RunWhenLoadFinished(() => waiting = false);
        yield return new WaitWhile(() => waiting);
        PlayerTurnStart();
    }
    
    // PHASE TRANSITIONS - Going in and out of the primary game phases.
    #region Phase Transitions

    private void PlayerTurnStart() {
        CurrentPhase = GamePhase.PlayerTurn;
        LogUtil.LogGameMessage("Player Turn Start");
        
        // Draw five tiles at the start of each turn.
        for (int i = 0; i < 5; i++) { CommandManager.EnqueueCommand(new DrawCommand()); }    
        
    }

    private void PlayerExecutionStart() {
        CurrentPhase = GamePhase.PlayerExecution;
        LogUtil.LogGameMessage("Player Execution Start");
    }

    private void PlayerEndOfTurnStart() {
        CurrentPhase = GamePhase.PlayerEndOfTurn;
        LogUtil.LogGameMessage("Player End Of Turn Start");
        
        // Clean list of tiles being placed.
        tilesBeingPlaced.Clear();
        
        // Discard any cards left in the player's hand.
        CommandManager.EnqueueCommand(new ActionCommand(DiscardHand, ActionDurations.PlayerEndOfTurnDiscard));
        // CommandManager.EnqueueCommand(new ActionCommand(PlayerTurnStart, 1f));
        CommandManager.EnqueueCommand(new ActionCommand(EnemyTurnStart, 1f));
        
    }

    private void EnemyTurnStart() {
        CurrentPhase = GamePhase.EnemyPlacing;
        LogUtil.LogGameMessage("Enemy Placing Start");
        
        // TODO: Make this instead loop through enemies.
        EnemyWordManager.StartCalculatingWordForEnemy(EnemyEntities[0], null);
    }

    private void EnemyExecutionStart() {
        CurrentPhase = GamePhase.EnemyExecution;
        LogUtil.LogGameMessage("Enemy Execution Start");
    }
    
    #endregion
    
    // ACTIONS - Actions the player can perform in game.
    #region Actions

    public void DrawATile() {
        
        // NO TILES LEFT - If the bag and discard pile are both empty, then there is no tile to draw.
        if (TileBag.Tiles.Count < 1 && DiscardPile.Tiles.Count < 1) {
            LogUtil.LogGameMessage("No tile to draw!");
            return;
        }
        
        // HAND FULL - If the player's hand has 10 tiles in it, then the player cannot draw another tile.
        if (HandController.TilesInHand >= 10) {
            LogUtil.LogGameMessage("Hand full! Cannot draw.");
            return;
        }

        // RESHUFFLE - If the bag is empty but the discard pile has tiles, shuffle in tiles from the 
        // discard pile, and then draw.
        if (TileBag.Tiles.Count < 1) {
            LogUtil.LogGameMessage("Bag empty. Shuffling in tiles from discard pile.");
            TileBag.ShuffleInTiles(DiscardPile.PullTilesFromDiscardPile());
        }

        // DRAW - Draw a tile from the bag and add it to the hand.
        HandController.CreateTile(TileBag.DrawTile());

    }

    public void DiscardHand() {

        HandController.DiscardHand();

    }
    
    
    #endregion
    
    // TILE PLACING - Getting tiles onto the board from the player's hand.
    #region Tile Placing

    public HandTileController TileBeingDragged { get; private set; }
    private Action returnToHandAction;

    private List<TileReadyToSet> tilesBeingPlaced = new List<TileReadyToSet>();
    private BoardResolution currentSuccessfulBoardResolution;

    // If the user makes a mouse button click while dragging, that can be used to release the tile. Hold their click
    // here until LateUpdate. If the click hasn't been used for something else related to Tile Placing, then that
    // click can be used to cancel dragging.
    private bool tileUnPlacedThisFrame;
    private bool unhandledMouseButtonWhileDragging;

    public void RegisterDraggedTile(HandTileController tile, Action startDraggingAction, Action newReturnToHandAction) {
        
        // If another tile was being dragged, unregister it.
        UnregisterDraggedTile(true);
        
        // Register this tile.
        startDraggingAction?.Invoke();
        TileBeingDragged = tile;
        returnToHandAction = newReturnToHandAction;
        
    }

    public void UnregisterDraggedTile(bool returnToHand) {
        UnregisterUnhandledMouseButtonWhileDragging();
        TileBeingDragged = null;
        if (returnToHand) {
            returnToHandAction?.Invoke();
        }
        returnToHandAction = null;
    }
    
    /// <summary>
    /// Let go of a dragged tile. If this is over a board space, attempt to place the tile. Otherwise, simply end
    /// the drag and let the tile return to the user's hand.
    /// </summary>
    public void ReleaseTile() {
        
        // If the player isn't hovering over a board space, return the tile to his or her hand.
        if (BoardManager.CurrentHoveredSpace == null) {
            UnregisterDraggedTile(true);
        }
        
        // Otherwise, try placing the tile on the board.
        else {
            PlaceATile(BoardManager.CurrentHoveredSpace);
        }
        
    }
    
    /// <summary>
    /// Attempt to place a tile on the board. 
    /// </summary>
    public void PlaceATile(BoardSpace space) {

        if (TileBeingDragged == null) return;

        bool placeSuccess = BoardManager.CanPlaceTileOnSpace(space, tilesBeingPlaced, out var placeFailureMessage);
        if (!placeSuccess) {
            UnregisterDraggedTile(true);
            LogUtil.LogGameMessage(placeFailureMessage);
            return;
        }
        
        // Communicate the updated state to the Space and Tile in question.
        space.StartPlacingTile(TileBeingDragged);
        TileBeingDragged.StartPlacing(space);
        
        // Update the Tiles Being Placed.
        tilesBeingPlaced.Add(new TileReadyToSet{TileController = TileBeingDragged, BoardSpace = space});
        
        // Set the tile to no longer be dragging.
        UnregisterDraggedTile(false);

        ThreadingUtil.Instance.RunLater(() => {
            if (tilesBeingPlaced.Count > 0) {
                GameContext.CanvasAtlas.UndoFightButton.Appear();
                CheckBoardForWordLegality();
            }
        }, 0.2f);

    }

    /// <summary>
    /// Undo the last-placed tile.
    /// </summary>
    public void UndoPlacement() {

        // If currently dragging a tile, release it.
        UnregisterDraggedTile(true);
        
        // Only do something if there are actually tiles being placed.
        if (tilesBeingPlaced.Count < 1) return;
        
        // Step backward.
        TakeBackPlacedTile(tilesBeingPlaced.Last().TileController);

    }

    /// <summary>
    /// Return a tile to the player's hand and the board space to empty.
    /// </summary>
    public void TakeBackPlacedTile(HandTileController tileController) {

        // Grab the Tile/BoardSpace combo from the given tile.
        TileReadyToSet tileReadyToSet = tilesBeingPlaced.FirstOrDefault(e => e.TileController == tileController);
        if (tileReadyToSet == null) return;
        
        // Update the Tile and BoardSpace controllers and remove the tile from the list.
        tileReadyToSet.BoardSpace.UndoPlacingTile();
        tileReadyToSet.TileController.UndoPlacement();
        tilesBeingPlaced.Remove(tileReadyToSet);
        
        // If there are no more tiles being placed, hide the undo button.
        if (tilesBeingPlaced.Count == 0) {
            GameContext.CanvasAtlas.UndoFightButton.Disappear();
        }

        // This is considered a click related to tile placement, so absorb click input that may be related to placement.
        tileUnPlacedThisFrame = true;
        UnregisterUnhandledMouseButtonWhileDragging();
        
        // If the tiles cannot currently be set (<1 legal word or >0 illegal words), remove the set button.
        CheckBoardForWordLegality(); 
        
    }

    private bool CheckBoardForWordLegality() {

        if (tilesBeingPlaced.Count < 1) return false;
        currentSuccessfulBoardResolution = GameContext.BoardManager.GetCurrentSuccessfulBoardResolution(tilesBeingPlaced);
        if (currentSuccessfulBoardResolution == null) GameContext.CanvasAtlas.SetFightButton.Disappear();
        else GameContext.CanvasAtlas.SetFightButton.Appear();
        return currentSuccessfulBoardResolution != null;

    }

    public bool HasUnhandledMouseButtonWhileDragging() {
        return unhandledMouseButtonWhileDragging;
    }
    
    public void RegisterUnhandledMouseButtonWhileDragging() {
        if (!tileUnPlacedThisFrame) unhandledMouseButtonWhileDragging = true;
    }

    private void UnregisterUnhandledMouseButtonWhileDragging() {
        unhandledMouseButtonWhileDragging = false;
    }

    public void DraggedTileLateUpdate() {
        tileUnPlacedThisFrame = false;
    }

    
    /// <summary>
    /// Takes all the tiles and SETs them, executing their effects.
    /// </summary>
    public void SetTiles() {

        // BOARD CHECK. Only proceed if the board has been evaluated and all current placements are legal.
        if (currentSuccessfulBoardResolution == null) return;
        
        // REMOVE SET BUTTON.
        GameContext.CanvasAtlas.SetFightButton.Disappear();
        
        // DROP DRAGGED TILE. If there is a dragged tile, it's too late to put it anywhere.
        UnregisterDraggedTile(true);
        
        // EXECUTION PHASE. Switch to the Player Execution phase, disallowing tile inputs.
        PlayerExecutionStart();

        // Impose a short wait to the CommandManager so the user can watch the tiles prime for a beat.
        CommandManager.EnqueueCommand(new WaitCommand {Duration = ActionDurations.TileSettingWait});
        
        // Loop through tiles...
        foreach (TileReadyToSet tile in tilesBeingPlaced) {
            
            // PRIMING. Tell tiles to switch to a "priming" visual state.
            tile.TileController.StartSettingPriming();
            
            // COMMAND. Send off a command to execute the setting and effect of this tile.
            TileCommand tileCommand = new TileCommand() {
                BoardResolution = currentSuccessfulBoardResolution,
                BoardSpace = tile.BoardSpace,
                TileController = tile.TileController
            };
            CommandManager.EnqueueCommand(tileCommand);

        }
        
        // END TURN. Enqueue End-of-Turn Command.
        CommandManager.EnqueueCommand(new ActionCommand(PlayerEndOfTurnStart, ActionDurations.PlayerEndOfTurn));

    }

    public void CreateBoardTileFromHandTile(HandTileController tileController) {

        TileReadyToSet tileReadyToSet = tilesBeingPlaced.FirstOrDefault(e => e.TileController == tileController);
        if (tileReadyToSet == null) { Debug.Log("FightManager.SwapHandTileForBoardTile error — that tile isn't being placed"); return; }
        BoardManager.CreateBoardTile(tileReadyToSet.BoardSpace, currentSuccessfulBoardResolution);

    }

    #endregion
    
    // EFFECT TARGETING - Applying effects to entities
    #region Effect Targeting

    public EntityController GetPlayerEntityController() {
        return PlayerEntity;
    }

    public List<EntityController> GetEnemyEntityControllers() {
        return EnemyEntities;
    }

    public EntityController GetRandomEnemy() {
        return EnemyEntities.RandomElement();
    }
    
    /// <summary>
    /// Returns which enemy gets affected by a single-target effect.
    /// </summary>
    public EntityController TargetEnemyFromBoardSpace(BoardSpace space) {
        return EnemyEntities[(int) ((float) space.Row / BoardManager.Rows * EnemyEntities.Count)];
    }
    
    #endregion
    
    // ENEMY PLAYS - Enemy playing words (or being unable to play words)
    #region Enemy Plays

    public void OnEnemyWordFound(BoardResolution resolution, EntityController controller) {
        resolution.SpacesBeingResolved.ForEach(e => {
            BoardManager.CreateBoardTile(e.BoardSpace, resolution);
        });
        
        // TODO: Instead, do the enemy's effect, then progress to the next enemy or end turn if there are no more enemies.
        CommandManager.EnqueueCommand(new ActionCommand(PlayerTurnStart, 1f));
    }
    
    public void OnEnemyNoWordFound(BoardResolution word, EntityController controller) {
        // TODO: Progress to next enemy.
        // TODO: Do something to illustrate that the enemy couldn't place a word.
    }
    
    #endregion
    

}
