﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoardSpaceDisplayer : MonoBehaviour {

    private BoardManager board;
    private BoardSpace space;

    public SpriteRenderer SpriteRenderer;
    public Gradient ColorByNormalizedDistanceFromMid;
    public GameObject OutlineObject;
    
    public void Initialize(BoardManager owningBoard, BoardSpace owningSpace) {
        board = owningBoard;
        space = owningSpace;
        bool oddSpace = (space.Column + space.Row) % 2 == 0;
        float colorProg = Mathf.Clamp01(transform.localPosition.magnitude / (board.DiagonalSize * 0.5f)) * 0.8f + 0.1f;
        colorProg += oddSpace ? 0.1f : -0.1f;
        SpriteRenderer.color = ColorByNormalizedDistanceFromMid.Evaluate(colorProg);
    }

    private void OnMouseDown() {
        
        // Only recognize left mouse button clicks.
        if (!Input.GetMouseButtonDown(0)) return;
        
        space.RegisterClick();
        
    }

    private void OnMouseOver() {
        board.CurrentHoveredSpace = space;
        OutlineObject.SetActive(true);
    }

    private void OnMouseExit() {
        board.CurrentHoveredSpace = null;
        OutlineObject.SetActive(false);
    }
    
}
