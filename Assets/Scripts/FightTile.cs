﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// A tile present during a fight. This is distinct from an OwnedTile in that it will remember changes made to it
/// during a fight, while an OwnedTile only knows about permanent effects.
/// TODO: Use FightTile for any temporary tile-altering effects.
/// </summary>
public class FightTile {

    public TileData TileData => OwnedTile.TileData;
    public OwnedTile OwnedTile;
    public string Letters;

    public FightTile(OwnedTile ownedTile) {
        OwnedTile = ownedTile;
        Letters = ownedTile.Letters;
    }

    public List<string> PossibleLetters() {
        return TileData.Letters.Split('/').ToList();
    }

    public string EffectString() {
        return "a";
    }

    public bool CanBeSelectedWithLetter(string letter) {
        List<string> possibleLetters = PossibleLetters();
        foreach (string pl in possibleLetters) {
            if (pl.Substring(0, 1) == letter) return true;
        }
        return false;
    }

}
