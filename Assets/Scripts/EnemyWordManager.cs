﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Schema;
using JetBrains.Annotations;
using Sirenix.Utilities;
using UnityEngine;

public class EnemyWordManager : MonoBehaviour {

    // RATE PARAMS. This is a WIP feature. Ultimately we will want to scale using a stopwatch.
    public int WordsToCheckPerFrame;
    
    // CALCULATION TIME. This manager has a preferred and max amount of time it will spend looking for a word to play.
    public float PreferredCalculationTime;    // If a word is found before this time, it will be used.
    public float MaxCalculationTime;          // If no word can be found before this time, the enemy will not play a word.
    private float calculationTimer;
    private bool isCalculating;

    private EntityController currentEnemyController;
    private EnemyData currentEnemyData;
    private List<string> enemyWords;

    private class EnemyWordScore {
        public BoardResolution Resolution;
        public float Score;
    }

    private List<EnemyWordScore> scoredWordsSoFar = new List<EnemyWordScore>();

    public delegate void WordFoundDelegate(BoardResolution word, EntityController controller);
    public event WordFoundDelegate OnWordFound;
    public event WordFoundDelegate OnNoWordFound;
    
    private void Awake() {
        GameContext.EnemyWordManager = this;
    }

    public void StartCalculatingWordForEnemy(EntityController enemyController, List<string> recentlyUsedWords) {

        // Setup Data Required for Calculation.
        isCalculating = true;
        currentEnemyController = enemyController;
        currentEnemyData = enemyController.EnemyData;
        scoredWordsSoFar = new List<EnemyWordScore>();
        queuedWordsToCheck = new List<string>();
        enemyWords = currentEnemyData.EnemyDictionary.Words.ToList();
        enemyWords.Shuffle();

        // Set Board Spaces.
        BoardManager.Spaces.ForEach(e => e.ForEach(f => { if (f.TileController != null) boardSpacesWithLetters.Add(f); }));
        boardSpacesWithLetters.Shuffle();

    }

    private void Update() {

        // NOT CALCULATING. Do nothing.
        if (!isCalculating) return;

        // TIME'S UP WITH NO WORD. Announce a null result.
        if (calculationTimer > MaxCalculationTime && scoredWordsSoFar.Count < 1) {
            OnNoWordFound?.Invoke(null, currentEnemyController);
            return;
        }
        
        // WORD FOUND. Announce the result.
        if (calculationTimer > PreferredCalculationTime && scoredWordsSoFar.Count > 0) {
            BoardResolution selectedResolution = scoredWordsSoFar.OrderByDescending(e => e.Score).First().Resolution;
            OnWordFound?.Invoke(selectedResolution, currentEnemyController);
            return;
        }
        
        // CALCULATION. Check legality and score more words.
        ScoreMoreWords();

    }
    
    #region Word Queueing
    
    private BoardManager BoardManager => GameContext.BoardManager;
    private List<BoardSpace> boardSpacesWithLetters = new List<BoardSpace>();
    private int lastBoardSpaceWithLetterIndex = -1;
    
    private List<string> queuedWordsToCheck = new List<string>();
    // private class WordToCheck {
    //     public BoardSpace BoardSpace;
    //     public string Word;
    // }

    private void QueueMoreWords() {

        // Proceed through the spaces with letters until one is found that can be crossed. Add all enemy words to 
        // be searched on that space.
        // TODO: Currently this assumes that enemies will ONLY be able to play words by crossing a letter, as opposed
        // TODO: to extending or playing alongside. Eventually this will be expanded. Also, enemies shall be able
        // TODO: to express "personalities" by opting to play alongside more or less often.
        while (queuedWordsToCheck.Count < 1) {
            // lastBoardSpaceWithLetterIndex++;
            // BoardSpace examinedSpace = boardSpacesWithLetters.RandomElement();
            // bool canCrossVertically = examinedSpace.GetVerticallyConnectedSpacesWithTiles().Count < 2;
            // bool canCrossHorizontally = examinedSpace.GetHorizontallyConnectedSpacesWithTiles().Count < 2;
            // if (!canCrossVertically && !canCrossHorizontally) continue;
            // string letterAtSpace = examinedSpace.TileController.SelectedLetter();
            // foreach (string enemyWord in enemyWords) {
            //     if (!enemyWord.Contains(letterAtSpace)) continue;    // Reject word if it doesn't contain the letter TODO: This is not needed if the word is played alongside.
            //     queuedWordsToCheck.Add(new WordToCheck {
            //         BoardSpace = examinedSpace,
            //         LetterAtSpace = letterAtSpace,
            //         Word = enemyWord,
            //         IsVert = canCrossVertically
            //     });
            // }
            queuedWordsToCheck.Add(enemyWords[0]);
            enemyWords.RemoveAt(0);
        }

    }
    
    #endregion

    #region Word Scoring
    
    private void ScoreMoreWords() {

        // CHECK WORDS.
        int wordsCheckedThisFrame = 0;
        while (wordsCheckedThisFrame < WordsToCheckPerFrame) {
            
            // EMPTY QUEUE. Queue more words.
            if (queuedWordsToCheck.Count < 1) {
                QueueMoreWords();
                
                // STILL EMPTY QUEUE. Ran out of words to check. Do nothing.
                if (queuedWordsToCheck.Count < 1) {
                    return;
                }
            }

            // MOVE TO NEXT WORD.
            wordsCheckedThisFrame++;
            string wordBeingChecked = queuedWordsToCheck[0];
            queuedWordsToCheck.RemoveAt(0);

            List<BoardResolution> resolutions = BoardManager.BruteFitWord(wordBeingChecked);
            foreach (BoardResolution resolution in resolutions) {
                scoredWordsSoFar.Add(new EnemyWordScore {
                    Resolution = resolution,
                    Score = Random.Range(0f, 1f)            // TODO: Score it based on the enemy's personality, E.G. preferring more links, closer to edges, etc.
                });
            }

        }
        
    }
    
    #endregion

    private void StopCalculation() {
        isCalculating = false;
        currentEnemyData = null;
        enemyWords.Clear();
        scoredWordsSoFar.Clear();
        queuedWordsToCheck.Clear();
        boardSpacesWithLetters.Clear();
        lastBoardSpaceWithLetterIndex = -1;
    }
    

}
