﻿using UnityEngine;

public class PlayerInputManager : MonoBehaviour {

    private void Awake() {
        GameContext.PlayerInputManager = this;
    }

    private void Update() {
        
        // Process Alphabetic Input
        if (Input.GetKeyDown(KeyCode.A)) AlphabeticInput("A"); if (Input.GetKeyDown(KeyCode.N)) AlphabeticInput("N");
        if (Input.GetKeyDown(KeyCode.B)) AlphabeticInput("B"); if (Input.GetKeyDown(KeyCode.O)) AlphabeticInput("O");
        if (Input.GetKeyDown(KeyCode.C)) AlphabeticInput("C"); if (Input.GetKeyDown(KeyCode.P)) AlphabeticInput("P");
        if (Input.GetKeyDown(KeyCode.D)) AlphabeticInput("D"); if (Input.GetKeyDown(KeyCode.Q)) AlphabeticInput("Q");
        if (Input.GetKeyDown(KeyCode.E)) AlphabeticInput("E"); if (Input.GetKeyDown(KeyCode.R)) AlphabeticInput("R");
        if (Input.GetKeyDown(KeyCode.F)) AlphabeticInput("F"); if (Input.GetKeyDown(KeyCode.S)) AlphabeticInput("S");
        if (Input.GetKeyDown(KeyCode.G)) AlphabeticInput("G"); if (Input.GetKeyDown(KeyCode.T)) AlphabeticInput("T");
        if (Input.GetKeyDown(KeyCode.H)) AlphabeticInput("H"); if (Input.GetKeyDown(KeyCode.U)) AlphabeticInput("U");
        if (Input.GetKeyDown(KeyCode.I)) AlphabeticInput("I"); if (Input.GetKeyDown(KeyCode.V)) AlphabeticInput("V");
        if (Input.GetKeyDown(KeyCode.J)) AlphabeticInput("J"); if (Input.GetKeyDown(KeyCode.W)) AlphabeticInput("W");
        if (Input.GetKeyDown(KeyCode.K)) AlphabeticInput("K"); if (Input.GetKeyDown(KeyCode.X)) AlphabeticInput("X");
        if (Input.GetKeyDown(KeyCode.L)) AlphabeticInput("L"); if (Input.GetKeyDown(KeyCode.Y)) AlphabeticInput("Y");
        if (Input.GetKeyDown(KeyCode.M)) AlphabeticInput("M"); if (Input.GetKeyDown(KeyCode.Z)) AlphabeticInput("Z");
        
        // Process Numeric Inputs
        if (Input.GetKeyDown(KeyCode.Alpha0) || Input.GetKeyDown(KeyCode.Keypad0)) { NumericInput(0); }
        if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1)) { NumericInput(1); }
        if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2)) { NumericInput(2); }
        if (Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Keypad3)) { NumericInput(3); }
        if (Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Keypad4)) { NumericInput(4); }
        if (Input.GetKeyDown(KeyCode.Alpha5) || Input.GetKeyDown(KeyCode.Keypad5)) { NumericInput(5); }
        if (Input.GetKeyDown(KeyCode.Alpha6) || Input.GetKeyDown(KeyCode.Keypad6)) { NumericInput(6); }
        if (Input.GetKeyDown(KeyCode.Alpha7) || Input.GetKeyDown(KeyCode.Keypad7)) { NumericInput(7); }
        if (Input.GetKeyDown(KeyCode.Alpha8) || Input.GetKeyDown(KeyCode.Keypad8)) { NumericInput(8); }
        if (Input.GetKeyDown(KeyCode.Alpha9) || Input.GetKeyDown(KeyCode.Keypad9)) { NumericInput(9); }
        
        // Process Backspace
        if (Input.GetKeyDown(KeyCode.Backspace)) { BackspaceInput(); }
        
        // Process Enter and Return
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter)) { EnterInput(); }
        
    }

    // ALPHABETIC
    private void AlphabeticInput(string letterInput) {
        if (GameContext.HandController != null) GameContext.HandController.ReceiveAlphabeticPlayerInput(letterInput);
    }
    
    // NUMERIC
    private void NumericInput(int numberInput) {
        if (GameContext.HandController != null) GameContext.HandController.ReceiveNumericPlayerInput(numberInput);
    }

    // BACKSPACE
    private void BackspaceInput() {
        if (GameContext.FightButtonActions != null) GameContext.FightButtonActions.UndoPlacement();
    }
    
    // ENTER and RETURN
    private void EnterInput() {
        if (GameContext.FightButtonActions != null) GameContext.FightButtonActions.SetTiles();
    }
    
}
