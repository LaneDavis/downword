﻿using TMPro;
using UnityEngine;

public class HandTileController : TileController {

    private FightManager FightManager => GameContext.FightManager;
    private HandController HandController => GameContext.HandController;
    private bool IsPlayerTurn => FightManager.CurrentPhase == FightManager.GamePhase.PlayerTurn;

    private TileTextGenerator textGenerator;
    private Transform myTransform;
    
    [Header("Display")]
    public TextMeshProUGUI MainEffectText;

    // STATUS - What this tile is currently doing.
    private bool isBeingDragged;
    private bool isBeingHovered;
    private bool isBeingPlaced;
    private bool isReadyToSet;
    private bool isSettingPriming;
    private bool isSettingExecuting;
    private bool isBeingDiscarded;
    
    // Hand Index and Position
    private int IndexInHand => HandController.GetIndexOfTile(FightTile);
    private Vector2 TargetPositionInHand => HandController.transform.position + new Vector3(HandController.OffsetOfTileIndex(IndexInHand), 0f);
    private int lastIndex;

    [Header("Selection")]
    public float SelectionClickTimeThreshold;
    public float SelectionDragDistanceThreshold;
    private bool isBeingSelectionJudged;
    private float selectionJudgementEndTime;
    private Vector2 selectionOriginalMouseLocation;

    [Header("Resting")]
    public float RestingTargetScale;
    public float RestingPositioningTime;
    public float RestingScaleTime;

    [Header("Dragging")]
    public Vector2 DraggingTargetOffset;
    public float DraggingTargetScale;
    public AnimationCurve DraggingPositioningTimeByTransitionProgress;
    public AnimationCurve DraggingScaleTimeByTransitionProgress;
    public AnimationCurve DraggingTransitionTimeByUnitDistanceFromMouse;
    private float draggingTransitionTime;
    private float dragTransitionProgress => !isBeingDragged ? 0f : Mathf.Clamp01((Time.time - dragStartTime) / draggingTransitionTime);
    private float dragStartTime;
    
    [Header("Placing Movement")]
    public float PlacingTime;
    public AnimationCurve PlacingScale;
    public AnimationCurve PlacingXPos;
    public AnimationCurve PlacingYPos;
    private float placingStartTime;
    private float placingEndTime;
    private float PlacingProgress => Mathf.Clamp01((Time.time - placingStartTime) / (placingEndTime - placingStartTime));
    private TransformData placingEndData;

    [Header("Ready to Set")]
    public AnimationCurve ReadyPulsePowerByPlacingProgress;
    public AnimationCurve ReadyPulseScale;
    public float ReadyPulsePower => isReadyToSet ? 1f : ReadyPulsePowerByPlacingProgress.Evaluate(PlacingProgress);

    [Header("Setting Priming")]
    public float SettingPrimingTime;
    public float SettingPrimingTargetScale;
    public AnimationCurve SettingPrimingScaleCurveByProgress;
    private float settingPrimingStartTime;
    private float SettingPrimingProgress => !isSettingPriming ? 0f : Mathf.Clamp01((Time.time - settingPrimingStartTime) / SettingPrimingTime);
    public FxController SettingPrimingShakeFx;

    [Header("Setting Executing")]
    public float SettingExecutingTime;
    public float SettingExecutingTargetScale;
    public AnimationCurve SettingExecutingScaleCurveByProgress;
    private float settingExecutingStartTime;
    private float SettingExecutingProgress => !isSettingExecuting ? 0f : Mathf.Clamp01((Time.time - settingExecutingStartTime) / SettingExecutingTime);

    [Header("Discarding")]
    public AnimationCurve DiscardingScaleMultiplierByDistanceFromDiscardPile;
    private float DiscardScalingTakeoverRadius => DiscardingScaleMultiplierByDistanceFromDiscardPile.LastKey().time;
    private Vector2 DiscardPilePosition => GameContext.DiscardPileDisplayer.transform.position;
    
    private Vector2 targetPosition;
    private TransformData previousTransformData;
    private Vector3 scaleVelocity;
    private Vector2 positioningVelocity;
    
    // Initialize display from a Fight Tile.
    public void Initialize(FightTile newOwnedTile) {
        FightTile = newOwnedTile;
        textGenerator = new TileTextGenerator(FightTile);
        MainEffectText.text = textGenerator.GetText();
        SetLetters();
        gameObject.name = "Hand Tile - In Hand " + IndexInHand.ToString("D2");
        myTransform = transform;
        previousTransformData = new TransformData(myTransform);
    }
    
    // START PLACING - Switch to a "Placing" mode in which the tile flies toward a target board space.
    public void StartPlacing(BoardSpace space) {
        
        // Set Status.
        isBeingPlaced = true;
        isBeingDragged = false;
        placingStartTime = Time.time;
        placingEndTime = Time.time + PlacingTime;
        
        // Set Transform Data for Start and End Positions;
        previousTransformData = new TransformData(transform);
        placingEndData = new TransformData() {
            Position = space.Displayer.transform.position,
            LocalScale = new Vector3(0.6f, 0.6f, 0.6f),
            Rotation = Quaternion.identity
        };
        
    }

    public void UndoPlacement() {
        
        // Cancel Previous Momentum.
        scaleVelocity = Vector3.zero;
        positioningVelocity = Vector2.zero;
        
        // Reset target position and scale.
        HandController.InsertTileIntoList(this, lastIndex);
        previousTransformData = new TransformData {
            Position = TargetPositionInHand,
            LocalScale = Vector3.one
        };
        
        // Set Status.
        isReadyToSet = false;
        isBeingPlaced = false;
        isBeingDragged = false;

    }

    public void DiscardTile() {
        
        // Set Status.
        isReadyToSet = false;
        isBeingPlaced = false;
        isBeingDragged = false;
        isBeingDiscarded = true;

    }

    private void Update() {

        targetPosition = Vector2.zero;
        
        // SELECTION JUDGMENT - Watch mouse movement to determine if the user is dragging or click-selecting.
        if (isBeingSelectionJudged) {
            
            // If the user has held the tile for a certain period, begin dragging it.
            if (Time.time > selectionJudgementEndTime) {
                StartDragging();
            }

            // If the user moves the tile outside of a certain range, begin dragging it.
            if (Vector2.Distance(GameContext.MainCamera.ScreenToWorldPoint(Input.mousePosition), selectionOriginalMouseLocation) > SelectionDragDistanceThreshold) {
                StartDragging();
            }
        }
        
        // DRAGGING - Follow the cursor.
        if (isBeingDragged) {
            UpdateDragPosition();
            
            bool transitionCompleted = dragTransitionProgress >= 1f;
            if (!transitionCompleted) {
                myTransform.localScale = Vector3.SmoothDamp(myTransform.localScale, Vector3.one * DraggingTargetScale, ref scaleVelocity, DraggingScaleTimeByTransitionProgress.Evaluate(dragTransitionProgress));
                myTransform.position = Vector2.SmoothDamp(myTransform.position, targetPosition, ref positioningVelocity, DraggingPositioningTimeByTransitionProgress.Evaluate(dragTransitionProgress));
            }
            else {
                myTransform.localScale = Vector3.one * DraggingTargetScale;
                myTransform.position = targetPosition;
            }

            // While dragging, clicking anything not related to tile placement releases the tile.
            if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)) FightManager.RegisterUnhandledMouseButtonWhileDragging();
        }

        // RESTING - Rest in position in hand.
        if (!isBeingDragged && !isBeingPlaced && !isReadyToSet && !isSettingPriming && !isSettingExecuting && !isBeingDiscarded) { 
            targetPosition = TargetPositionInHand;
            myTransform.localScale = Vector3.SmoothDamp(myTransform.localScale, Vector3.one * RestingTargetScale, ref scaleVelocity, RestingScaleTime);
            myTransform.position = Vector2.SmoothDamp(myTransform.position, targetPosition, ref positioningVelocity, RestingPositioningTime);
        }
        
        // DISCARDED - Move toward the discard pile, then return to pool.
        if (isBeingDiscarded) {
            targetPosition = DiscardPilePosition;
            float distanceFromDiscardPile = Vector2.Distance(myTransform.position, DiscardPilePosition);
            myTransform.localScale = distanceFromDiscardPile > DiscardScalingTakeoverRadius ? Vector3.SmoothDamp(myTransform.localScale, Vector3.one * RestingTargetScale, ref scaleVelocity, RestingScaleTime) : Vector3.SmoothDamp(myTransform.localScale, RestingTargetScale * DiscardingScaleMultiplierByDistanceFromDiscardPile.Evaluate(distanceFromDiscardPile) * Vector3.one, ref scaleVelocity, 0.0167f);
            myTransform.position = Vector2.SmoothDamp(myTransform.position, targetPosition, ref positioningVelocity, RestingPositioningTime);
        }

        // PLACING or READY - Move toward target tile and pulse when ready to set.
        if (isBeingPlaced || isReadyToSet) {
            UpdatePlacingPosition();
            UpdatePlacingAndReadyScale();
            if (PlacingProgress >= 1) {
                isReadyToSet = true;
            }
        }
        
        // SETTING PRIMING — Displaying that a "set" action is taking place
        if (isSettingPriming) {
            myTransform.localScale = Vector3.LerpUnclamped(previousTransformData.LocalScale, SettingPrimingTargetScale * Vector3.one, SettingPrimingScaleCurveByProgress.Evaluate(SettingPrimingProgress));
            SettingPrimingShakeFx.ToggleAll(true, null, SettingPrimingProgress);
        }
        
        // SETTING EXECUTING — Driving the tile into the board, then replacing itself with a lighter-weight
        // board tile controller.
        if (isSettingExecuting) {
            myTransform.localScale = Vector3.LerpUnclamped(previousTransformData.LocalScale, SettingExecutingTargetScale * Vector3.one, SettingExecutingScaleCurveByProgress.Evaluate(SettingExecutingProgress));
            if (SettingExecutingProgress >= 1f) {
                FightManager.CreateBoardTileFromHandTile(this);
                ReturnTileToPool();
            }
        }
        
        // Update Hand Index
        if (IndexInHand > -1) lastIndex = IndexInHand;
        
        // Set Z Offset
        float newZ = ObjectZs.HandTiles + (isBeingDragged ? ObjectZs.HandTileDraggedOffset : ObjectZs.HandTilePerIndexOffset * lastIndex);
        myTransform.SetZPos(newZ);
        
    }

    // LATE UPDATE — Handling things that must occur after Update
    private void LateUpdate() {
        bool wasBeingDragged = isBeingDragged;
        if (isBeingDragged && FightManager.HasUnhandledMouseButtonWhileDragging()) {
            FightManager.UnregisterDraggedTile(true);
        }
        if (wasBeingDragged) {
            FightManager.DraggedTileLateUpdate();
        }
    }

    // ON MOUSE OVER — Update that only happens while the mouse is hovered over the tile.
    private void OnMouseOver() {
        if (Input.GetMouseButtonDown(1)) {
            if (isReadyToSet) FightManager.TakeBackPlacedTile(this);
        }
    }

    private void UpdateDragPosition() {
        
        // Find the Mouse.
        Vector2 mousePos = GameContext.MainCamera.ScreenToWorldPoint(Input.mousePosition);
        
        // Set the tile's target position.
        targetPosition = mousePos + DraggingTargetOffset;
        
    }

    private void UpdatePlacingPosition() {

        float posX = Mathf.Lerp(previousTransformData.Position.x, placingEndData.Position.x, PlacingXPos.Evaluate(PlacingProgress));
        float posY = Mathf.Lerp(previousTransformData.Position.y, placingEndData.Position.y, PlacingYPos.Evaluate(PlacingProgress));
        transform.position = new Vector3(posX, posY, ObjectZs.ReadyToPlaceTiles);

    }

    private void UpdatePlacingAndReadyScale() {

        Vector3 finalScale = Vector3.LerpUnclamped(previousTransformData.LocalScale, placingEndData.LocalScale, PlacingScale.Evaluate(PlacingProgress));
        float readyPulseDiff = (1f - ReadyPulseScale.Evaluate(Time.time)) * ReadyPulsePower;
        finalScale *= 1f - readyPulseDiff;
        transform.localScale = finalScale;
        
    }
    
    private void OnMouseDown() {
        
        // The user can't drag the tile while it is flying into its placed position.
        if (isBeingPlaced && !isReadyToSet) return;    
        
        // Only recognize left mouse button clicks.
        if (!Input.GetMouseButtonDown(0)) return;
        
        // Start the process of judging whether this is a drag or a click selection.
        isBeingSelectionJudged = true;
        selectionJudgementEndTime = Time.time + SelectionClickTimeThreshold;
        selectionOriginalMouseLocation = GameContext.MainCamera.ScreenToWorldPoint(Input.mousePosition);

    }

    public void StartDragging() {

        if (!IsPlayerTurn) return;                     // TURN CHECK. Can't start dragging if it's not the player's turn.
        if (isBeingDragged) return;                    // ALREADY DRAGGING. Can't start dragging if it is already being dragged.
        if (isBeingPlaced && !isReadyToSet) return;    // FLYING. Can't start dragging while tile is being placed.
        if (isBeingDiscarded) return;                  // DISCARDED. This tile is on the way to the discard pile.

        if (isReadyToSet) {
            FightManager.TakeBackPlacedTile(this);
        }
        
        isBeingSelectionJudged = false;
        draggingTransitionTime = DraggingTransitionTimeByUnitDistanceFromMouse.Evaluate(Vector2.Distance(GameContext.MainCamera.ScreenToWorldPoint(Input.mousePosition), myTransform.position));
        dragStartTime = Time.time;
        FightManager.RegisterDraggedTile(this, () => isBeingDragged = true, ReleaseAndReturnToHand);

    }

    private void OnMouseUp() {

        // SELECTION JUDGEMENT - If the user executes a rapid click and release, start dragging the tile
        if (isBeingSelectionJudged && Time.time < selectionJudgementEndTime) {
            StartDragging();
            return;
        }
        
        // Otherwise, attempt to place the tile.
        if (!isBeingDragged) return;
        FightManager.ReleaseTile();
        
    }

    private void ReleaseAndReturnToHand() {
        
        // Cancel Previous Momentum.
        scaleVelocity = Vector3.zero;
        positioningVelocity = Vector2.zero;
        
        // Re-Add the tile to the player's hand.
        HandController.InsertTileIntoList(this, lastIndex);
        
        // Set the tile's current transform for lerping.
        previousTransformData = new TransformData {
            Position = TargetPositionInHand,
            LocalScale = Vector3.one
        };
        isBeingDragged = false;
        
    }

    /// <summary>
    /// Begin transitioning toward setting. All the tiles rise up at once so they can smash into the board one
    /// by one as they execute their effects.
    /// </summary>
    public void StartSettingPriming() {

        // Set State
        isBeingDragged = isBeingHovered = isBeingPlaced = isBeingSelectionJudged = isReadyToSet = false;
        isSettingPriming = true;
        
        // Record Previous Transform (so the tile can lerp from that to the "primed" state).
        previousTransformData = new TransformData(myTransform);
        settingPrimingStartTime = Time.time;
        
        // Add this tile to the discard pile.
        GameContext.DiscardPile.AddTile(FightTile);

    }
    
    /// <summary>
    /// End the priming phase and drive the tile into the board. 
    /// </summary>
    public void StartSettingExecuting() {

        // Set State
        SettingPrimingShakeFx.ToggleAll(false);
        isBeingDragged = isBeingHovered = isBeingPlaced = isBeingSelectionJudged = isReadyToSet = isSettingPriming = false;
        isSettingExecuting = true;
        
        // Record Previous Transform (so the tile can lerp from that to the "executed" state).
        previousTransformData = new TransformData(myTransform);
        settingExecutingStartTime = Time.time;

    }

    /// <summary>
    /// When a tile disappears, it is fully reset and then returned to the TilePool.
    /// </summary>
    public void ReturnTileToPool() {
        
        isBeingDragged = isBeingHovered = isBeingPlaced = isBeingSelectionJudged = isReadyToSet = isSettingPriming = isSettingExecuting = isBeingDiscarded = false;
        GameContext.TilePool.ReturnHandTileToPool(this);
        
    }
    
}
