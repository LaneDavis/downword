﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HandController : MonoBehaviour {

    private FightManager FightManager => GameContext.FightManager;
    
    private List<HandTileController> tileControllers = new List<HandTileController>();
    public int TilesInHand => tileControllers.Count;
    
    // Positioning
    public float PreferredSpacing;
    public float WidthLimit;

    // Hand Adjustment
    public float HeightDiffToRemoveFromHand;
    
    // Selection with Letters
    private string lastLetterSelection;
    private int lastLetterSelectionIdx;
    
    private void Awake() {
        GameContext.HandController = this;
    }

    public void CreateTile(FightTile tileData) {
        
        // Pull a tile from the tile pool.
        HandTileController newHandTile = GameContext.TilePool.GrabHandTile();
        
        // Position it at the bag displayer and activate it.
        newHandTile.transform.position = GameContext.BagDisplayer.transform.position;
        newHandTile.gameObject.SetActive(true);
        
        // Tell it what to look like.
        newHandTile.Initialize(tileData);
        
        // Add it to the hand.
        tileControllers.Add(newHandTile);
        
    }

    public void DiscardHand() {
        for (int i = tileControllers.Count - 1; i >= 0; i--) {
            DiscardTile(tileControllers[i]);
        }
    }

    public void DiscardTile(HandTileController tileController) {
        RemoveTileFromList(tileController);
        GameContext.DiscardPile.AddTile(tileController.FightTile);
        tileController.DiscardTile();
    }

    public int GetIndexOfTile(FightTile fightTile) {
        for (int i = 0; i < tileControllers.Count; i++) {
            if (fightTile == tileControllers[i].FightTile) return i;
        }
        return -1;
    }

    public HandTileController GetTileAtIndex(int index) {
        return tileControllers.Count > index ? tileControllers[index] : null;
    }

    public float OffsetOfTileIndex(int index) {
        bool boundedByWidthLimit = (tileControllers.Count - 1) * PreferredSpacing > WidthLimit;
        float spacingPerTile = boundedByWidthLimit ? (WidthLimit / (tileControllers.Count - 1)) : PreferredSpacing;
        float leftmost = -(tileControllers.Count - 1) * spacingPerTile / 2f;
        return leftmost + index * spacingPerTile;
    }

    public int FindNearestIndexToPoint(Vector2 point) {
        int bestIndexSoFar = -1;
        float bestDistSoFar = 10000f;
        Vector2 rootPos = transform.position;
        for (int i = 0; i < tileControllers.Count; i++) {
            Vector2 indexPoint = rootPos + Vector2.right * OffsetOfTileIndex(i);
            float dist = Vector2.Distance(point, indexPoint);
            if (bestDistSoFar > dist) {
                bestIndexSoFar = i;
                bestDistSoFar = dist;
            }
        }
        return bestIndexSoFar;
    }

    public void ReceiveAlphabeticPlayerInput(string letterInput) {

        // Update selection index to loop through legal letters.
        if (lastLetterSelection == letterInput) {
            lastLetterSelectionIdx++;
        }
        else {
            lastLetterSelection = letterInput;
            lastLetterSelectionIdx = 0;
        }
        
        // Grab tiles that can be selectable with this letter.
        List<HandTileController> selectableTiles = tileControllers.Where(e => e.FightTile.CanBeSelectedWithLetter(letterInput)).ToList();
        if (selectableTiles.Count > 0) selectableTiles[lastLetterSelectionIdx % selectableTiles.Count].StartDragging();

    }

    public void ReceiveNumericPlayerInput(int numberInput) {
        
        // The Hand Displayer's tiles start at idx = 0, but it's easier for the player if they correspond to input
        // starting at 1. For this reason, subtract 1 from the numberInput (and make input 0 equal index 9)
        numberInput--;
        if (numberInput < 0) numberInput = 9;
        
        // If there is a tile at the chosen index, select that tile and start dragging it.
        HandTileController selectedTile = GetTileAtIndex(numberInput);
        if (selectedTile != null) selectedTile.StartDragging();
        
    }

    private void Update() {

        HandTileController draggedTile = FightManager.TileBeingDragged;
        if (draggedTile == null) return;
        
        // Remove from hand if far enough up.
        if (tileControllers.Contains(draggedTile) && draggedTile.transform.position.y > transform.position.y + HeightDiffToRemoveFromHand) {
            RemoveTileFromList(draggedTile);
        }

        // Otherwise, reposition tiles around the dragged tile.
        else if (tileControllers.Contains(draggedTile) || !tileControllers.Contains(draggedTile) && draggedTile.transform.position.y <= transform.position.y + HeightDiffToRemoveFromHand) {
            InsertTileIntoList(draggedTile, 0);    // Ensure dragged tile is in the list.
            int newIdx = FindNearestIndexToPoint(GameContext.MainCamera.ScreenToWorldPoint(Input.mousePosition));
            MoveTile(draggedTile, newIdx);
        }

    }

    private void RemoveTileFromList(HandTileController tileController) {
        if (tileControllers.Contains(tileController)) {
            tileControllers.Remove(tileController);
        }
    }

    public void InsertTileIntoList(HandTileController tileController, int insertionIndex) {
        if (!tileControllers.Contains(tileController)) {
            tileControllers.Insert(Math.Min(tileControllers.Count, insertionIndex), tileController);
        }
    }

    private void MoveTile(HandTileController tileController, int insertionIndex) {
        if (tileControllers.Contains(tileController)) {
            RemoveTileFromList(tileController);
        }
        InsertTileIntoList(tileController, insertionIndex);
    }
    
}
