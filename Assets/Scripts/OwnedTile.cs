﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A tile that the player owns. This is distinct from a TileData in that it will remember changes made to it throughout
/// a run, while a TileData is immutable.
/// TODO: Use OwnedTile for any permanent tile-altering effects.
/// </summary>
public class OwnedTile {

    public TileData TileData;
    public string Letters;

    public OwnedTile(TileData tileData) {
        TileData = tileData;
        Letters = tileData.Letters;
    }
    
}
