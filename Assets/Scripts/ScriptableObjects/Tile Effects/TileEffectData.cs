﻿using UnityEngine;

public class TileEffectData : ScriptableObject {
    
    public enum EffectTarget { SingleEnemy, AllEnemies, RandomEnemy, Self }
    public EffectTarget Target;
    public float Duration;

    public bool HasInstantEffect;
    public bool HasOngoingEffect;
    
    public virtual EntityEffect ExecutionEffect() {
        return new EntityEffect();    // Return a null effect that does nothing.
    }
    
}
