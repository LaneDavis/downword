﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data Object/Tile Effects/Block")]
public class TileEffectBlock : TileEffectData {
    
    public int Amount;
    // TODO: Graphic.
    // TODO: Sound.

    public override EntityEffect ExecutionEffect() {
        
        return new EntityEffect {
            BlockApplied = Amount
        };

    }
    
}
