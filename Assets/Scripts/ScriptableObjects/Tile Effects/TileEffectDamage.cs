﻿using UnityEngine;

[CreateAssetMenu(menuName = "Data Object/Tile Effects/Damage")]
public class TileEffectDamage : TileEffectData {

    public int Amount;
    // TODO: Graphic.
    // TODO: Sound.

    public override EntityEffect ExecutionEffect() {
        
        return new EntityEffect {
            DamageApplied = Amount
        };
        
    }
    
}
