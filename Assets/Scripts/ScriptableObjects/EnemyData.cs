﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data Object/Enemy Data")]
public class EnemyData : ScriptableObject  {
    
    public int MaxHP;
    public GameObject DisplayPrefab;
    public EnemyDictionary EnemyDictionary;

}
