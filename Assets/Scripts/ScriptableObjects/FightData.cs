﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Data Object/Fight Data")]
public class FightData : ScriptableObject {

    public List<EnemyData> Enemies;
    public enum FightType { Hallway, Elite, Boss }

    public FightType Type = FightType.Hallway;

}
