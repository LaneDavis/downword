﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data Object/Rarity Data")]
public class RarityData : ScriptableObject {

    public string DisplayName;
    public int PriceMin;
    public int PriceMax;

}
