﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "Data Object/Tile Data")]
public class TileData : ScriptableObject {

    [Tooltip("Examples: A; T/R; ING")]
    public string Letters;

    [Header("Effects")]
    public List<TileEffectData> Effects;
    
    [Header("Rarity")]
    public RarityData Rarity;

    [Header("Timing")]
    public float ExecutionDuration;

    public OwnedTile ToOwnedTile() {
        return new OwnedTile(this);
    }

}
