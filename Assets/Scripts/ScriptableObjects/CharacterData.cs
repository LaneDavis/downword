﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data Object/Character Data")]
public class CharacterData : ScriptableObject {

    public string DisplayName;
    public int StartingHP;
    public List<TileData> StartingTiles;
    public List<RelicData> StartingRelics;
    public GameObject DisplayPrefab;

}
